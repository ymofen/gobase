package gobase_test

import (
	"gitee.com/ymofen/gobase"
	"testing"
	"time"
)

func TestStrMap(t *testing.T) {
	strMap := gobase.NewStrMap()
	s := "120.253.226.97:8001=120.253.226.97:8002\nYDCGCS2000=CGCS2000\nQXCGCS2000=CGCS2000\n"
	strMap.ParseKVPairs(s, "=", "\n")
	for i := 0; i < 10; i++ {
		go func() {
			for {
				s0 := strMap.Encode("=", "\n")
				s1 := strMap.ExecReplace("120.253.226.97:8001,YDCGCS2000,WGS84")

				if len(s0) == 0 || s1 != "120.253.226.97:8002,CGCS2000,WGS84" {
					t.Fatalf("err")
				}
				time.Sleep(time.Millisecond)
			}
		}()
	}

	select {}

}
