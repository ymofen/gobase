package subpub

const (

	// action, &isOk, confMap
	ACTION_BEFORE_CONNECT = 1

	// actioin[,fromaddr[,data]]
	ACTION_CONNECTED = 2

	// action, buf
	ACTION_RECVBUF = 3
	// action, buf
	ACTION_SEND_BUF = 4

	// action, reason
	ACTION_DISCONNECTED = 5

	// ACTION, msg
	ACTION_START = 8

	// action, msg
	ACTION_CLOSE = 9

	// action, err
	ACTION_CONNECTFAIL = 10

	// action, topic, data
	ACTION_RECV_TOPIC_DATA = 11

	// action, err
	ACTION_ERROR   = 21
	ACTION_MESSAGE = 20
	// action, string
	ACTION_SUCCESSFUL = 22

	ACTION_CUSTOM = 200

	ACTION_SERVER_BASE = 30

	// action, sessionid, fromaddr[,data]
	ACTION_SERVER_CONN_CONNECTED = ACTION_SERVER_BASE + 1

	// action, sessionid, buf
	ACTION_SERVER_CONN_SEND_BUF = ACTION_SERVER_BASE + 2

	// action, sessionid, buf
	ACTION_SERVER_CONN_RECV_BUF = ACTION_SERVER_BASE + 3

	// action, sessionid, reason
	ACTION_SERVER_CONN_DISCONNECTED = ACTION_SERVER_BASE + 4
)
