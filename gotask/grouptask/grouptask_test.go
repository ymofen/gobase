package grouptask

import (
	"github.com/stretchr/testify/require"
	"log"
	"runtime"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

type AliveObjectTemp struct {
	idx int
}

func (this *AliveObjectTemp) doAny() {
	this.idx++
	log.Printf("%s", time.Now().String())
}

var (
	AliveObjectTempN int32
)

func doInner(flag int) {
	obj0 := &AliveObjectTemp{}
	atomic.AddInt32(&AliveObjectTempN, 1)
	runtime.SetFinalizer(obj0, func(obj interface{}) {
		atomic.AddInt32(&AliveObjectTempN, -1)
	})

	if flag == 0 {
		obj0.doAny()
	} else {
		DefaultGroupTask().PostTaskFunc(obj0, obj0.doAny)
	}

	obj0 = nil
}

func TestAliveObject(t *testing.T) {
	doInner(0)
	runtime.GC()
	time.Sleep(time.Second * 2)
	runtime.GC()
	require.Equal(t, int32(0), AliveObjectTempN)
}

func TestAliveObject2(t *testing.T) {

	for i := 0; i < 10; i++ {
		DefaultGroupTask().Stop()
		DefaultGroupTask().ConfigMaxWork(2)
		DefaultGroupTask().Start()
	}

	doInner(1)
	time.Sleep(time.Second * 2)
	DefaultGroupTask().cleanup(1)
	runtime.GC()
	time.Sleep(time.Second * 2)
	runtime.GC()
	require.Equal(t, int32(0), AliveObjectTempN)
}

func TestGroupTaskUsage(t *testing.T) {
	DefaultGroupTask().Stop()
	DefaultGroupTask().ConfigMaxWork(2)
	DefaultGroupTask().Start()
	var wg sync.WaitGroup

	n := 0
	for i := 0; i < 1000; i++ {
		wg.Add(1)
		DefaultGroupTask().PostTaskFuncArgs(0, func(args ...interface{}) {
			n++
			if n != args[0].(int)+1 {
				t.Errorf("group err")
			}
			wg.Done()
		}, i)
	}
	wg.Wait()

	wg.Add(1)
	ok, _ := DefaultGroupTask().PostTaskFuncAndWait(0, time.Second, func() {
		wg.Done()
	})
	if !ok {
		wg.Done()
	}
	t.Logf("%s", DefaultGroupTask().GroupStatus(0))
	t.Logf("%s", DefaultGroupTask().StatusString())
	t.Logf("%s", DefaultGroupTask().StatusSimpleString())

	DefaultGroupTask().Stop()

}

//
//func TestMultiG(t *testing.T) {
//	kv := make(map[string]int)
//
//	for i := 0; i < 10; i++ {
//		go func() {
//			for {
//				grouptask.DefaultGroupTask().PostTaskFunc("datasourceconf", func() {
//					kv[gobase.RandKeyString(24)] = rand.Intn(25)
//				})
//				//kv[RandKeyString(24)] = rand.Intn(100)
//				time.Sleep(time.Millisecond)
//			}
//		}()
//	}
//
//	// DefaultWorkers().ConfigChannelMaxWorkNum("datasourceconf", 3)
//
//	select {}
//
//}
//
//// 确保投递成功的次数和执行次数一样多
//// 如果投递执行的任务失败, 有可能会执行次数不完整
//func TestMultiMust(t *testing.T) {
//	workers := grouptask.NewGroupTask()
//	workers.ConfigMinWork(2)
//	workers.ConfigMaxWork(128)
//	workers.Start()
//
//	sesslst := make([]string, 100000)
//	for i := 0; i < len(sesslst); i++ {
//		sesslst[i] = gobase.RandHexString(24)
//	}
//
//	var wg sync.WaitGroup
//
//	var postN, failN, execN int32
//
//	var doOnce = func(duration time.Duration) {
//		defer func() {
//			wg.Done()
//		}()
//		t0 := time.Now()
//		for time.Since(t0) < duration {
//			idx := rand.Intn(1000000)
//			sessid := sesslst[idx%len(sesslst)]
//			wg.Add(1)
//			err := workers.PostTaskFunc(sessid, func() {
//				atomic.AddInt32(&execN, 1)
//				wg.Done()
//			})
//			if err == nil {
//				atomic.AddInt32(&postN, 1)
//			} else {
//				wg.Done()
//				atomic.AddInt32(&failN, 1)
//			}
//			time.Sleep(0)
//		}
//	}
//
//	idx := 0
//
//	var outmsg = func(prefix string) {
//		t.Logf("%s, postN:%d, execN:%d, failN:%d, worker:%s", prefix, postN, execN, failN, workers.StatusSimpleString())
//	}
//
//	go func() {
//		for {
//			time.Sleep(time.Second * 10)
//			outmsg(fmt.Sprintf("%d do", idx))
//		}
//	}()
//
//	for {
//		idx++
//		t0 := time.Now()
//		for i := 0; i < 1000; i++ {
//			wg.Add(1)
//			go doOnce(time.Second * time.Duration(rand.Intn(30)+30))
//		}
//		wg.Wait()
//		ms := time.Since(t0).Milliseconds()
//
//		outmsg(fmt.Sprintf("%d done, %d/ms", idx, int64(execN)/ms))
//
//		execN = 0
//		postN = 0
//		failN = 0
//	}
//
//	select {}
//
//}
//
//// 确保投递成功的次数和执行次数一样多
//func TestMultiMustBlock(t *testing.T) {
//	workers := grouptask.NewGroupTask()
//	workers.ConfigMinWork(2)
//	workers.ConfigMaxWork(128)
//	workers.Start()
//
//	sesslst := make([]string, 100000)
//	for i := 0; i < len(sesslst); i++ {
//		sesslst[i] = gobase.RandHexString(24)
//	}
//
//	var wg sync.WaitGroup
//
//	var postN, failN, execN int32
//
//	var doOnce = func(duration time.Duration) {
//		defer func() {
//			wg.Done()
//		}()
//		t0 := time.Now()
//		for time.Since(t0) < duration {
//			idx := rand.Intn(1000000)
//			sessid := sesslst[idx%len(sesslst)]
//			wg.Add(1)
//			err := workers.PostTaskFunc(sessid, func() {
//				atomic.AddInt32(&execN, 1)
//				wg.Done()
//			})
//			if err == nil {
//				atomic.AddInt32(&postN, 1)
//			} else {
//				wg.Done()
//				atomic.AddInt32(&failN, 1)
//			}
//			time.Sleep(0)
//		}
//	}
//
//	idx := 0
//	t0 := time.Now()
//	var outmsg = func(prefix string) {
//		ms := time.Since(t0).Milliseconds()
//		t.Logf("%s, postN:%d, execN:%d, failN:%d, %d/s, worker:%s", prefix, postN, execN, failN, int64(atomic.LoadInt32(&execN))*1000/ms, workers.StatusSimpleString())
//	}
//
//	go func() {
//		for {
//			time.Sleep(time.Second * 10)
//			outmsg(fmt.Sprintf("%d do", idx))
//		}
//	}()
//
//	for {
//		idx++
//		t0 = time.Now()
//		for i := 0; i < 1000; i++ {
//			wg.Add(1)
//			go doOnce(time.Second * time.Duration(rand.Intn(30)+5))
//		}
//		wg.Wait()
//		outmsg(fmt.Sprintf("%d done", idx))
//
//		execN = 0
//		postN = 0
//		failN = 0
//	}
//
//	select {}
//
//}
//
//func TestMultiWaitFor(t *testing.T) {
//}
//
///*
//channel 使用SyncMap
//
//	grouptask_test.go:77: n:1528688,  speed:764082.65/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:27, cmdpost-chan-size:21/16384, pushfail:0
//	grouptask_test.go:77: n:1656372,  speed:828142.11/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:8, cmdpost-chan-size:2/16384, pushfail:0
//	grouptask_test.go:77: n:1623984,  speed:811912.51/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:89, cmdpost-chan-size:86/16384, pushfail:0
//	grouptask_test.go:77: n:1633343,  speed:816560.69/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:103, cmdpost-chan-size:101/16384, pushfail:0
//	grouptask_test.go:77: n:1564000,  speed:781940.30/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:133, cmdpost-chan-size:130/16384, pushfail:0
//	grouptask_test.go:77: n:1543941,  speed:771899.72/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:6, cmdpost-chan-size:3/16384, pushfail:0
//	grouptask_test.go:77: n:1510630,  speed:754998.62/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:5, cmdpost-chan-size:2/16384, pushfail:0
//	grouptask_test.go:77: n:1531345,  speed:765414.29/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:24, cmdpost-chan-size:16/16384, pushfail:0
//	grouptask_test.go:77: n:1553268,  speed:776331.62/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:14, cmdpost-chan-size:14/16384, pushfail:0
//	grouptask_test.go:77: n:1601257,  speed:800245.86/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:3, cmdpost-chan-size:2/16384, pushfail:0
//	grouptask_test.go:77: n:1530204,  speed:764740.97/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:123, cmdpost-chan-size:118/16384, pushfail:0
//	grouptask_test.go:77: n:1601519,  speed:800747.25/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:7, cmdpost-chan-size:5/16384, pushfail:0
//	grouptask_test.go:77: n:1454361,  speed:727067.66/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:6, cmdpost-chan-size:3/16384, pushfail:0
//	grouptask_test.go:77: n:1505240,  speed:752520.59/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:28, cmdpost-chan-size:15/16384, pushfail:0
//	grouptask_test.go:77: n:1329494,  speed:664702.66/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:189, cmdpost-chan-size:187/16384, pushfail:0
//	grouptask_test.go:77: n:1288144,  speed:641748.77/s, info:group:49999, workers[2]:busy/n/min/max:4/21/20/512, chan:78/32768, terminated:0, task:528, cmdpost-chan-size:502/16384, pushfail:0
//	grouptask_test.go:77: n:1360428,  speed:680021.72/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:9, cmdpost-chan-size:0/16384, pushfail:0
//	grouptask_test.go:77: n:1390248,  speed:694797.24/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:89, cmdpost-chan-size:75/16384, pushfail:0
//	grouptask_test.go:77: n:1388193,  speed:693834.09/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:1295, cmdpost-chan-size:1303/16384, pushfail:0
//	grouptask_test.go:77: n:1380889,  speed:687349.67/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:4720, cmdpost-chan-size:4710/16384, pushfail:0
//	grouptask_test.go:77: n:1391765,  speed:695650.95/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:167, cmdpost-chan-size:159/16384, pushfail:0
//	grouptask_test.go:77: n:1397094,  speed:698249.82/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:1973, cmdpost-chan-size:2009/16384, pushfail:0
//	grouptask_test.go:77: n:1321857,  speed:660890.70/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:190, cmdpost-chan-size:188/16384, pushfail:0
//	grouptask_test.go:77: n:1314296,  speed:657111.07/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:109, cmdpost-chan-size:105/16384, pushfail:0
//	grouptask_test.go:77: n:1377914,  speed:688818.41/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:47, cmdpost-chan-size:47/16384, pushfail:0
//
//2023-11-11 18:05:01 使用 chanel 改成map + lk
//
//	grouptask_test.go:118: n:1605635,  speed:802722.62/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:2722, cmdpost-chan-size:2803/16384, pushfail:0
//	grouptask_test.go:118: n:1708078,  speed:853746.38/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:2/32768, terminated:0, task:1658, cmdpost-chan-size:1676/16384, pushfail:0
//	grouptask_test.go:118: n:1705777,  speed:852606.76/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:467, cmdpost-chan-size:464/16384, pushfail:0
//	grouptask_test.go:118: n:1675967,  speed:837755.92/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:51, cmdpost-chan-size:45/16384, pushfail:0
//	grouptask_test.go:118: n:1674811,  speed:837297.61/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:117, cmdpost-chan-size:109/16384, pushfail:0
//	grouptask_test.go:118: n:1647350,  speed:823277.85/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:35, cmdpost-chan-size:31/16384, pushfail:0
//	grouptask_test.go:118: n:1664638,  speed:832257.54/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:2574, cmdpost-chan-size:2600/16384, pushfail:0
//	grouptask_test.go:118: n:1677744,  speed:838871.37/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:8/32768, terminated:0, task:88, cmdpost-chan-size:44/16384, pushfail:0
//	grouptask_test.go:118: n:1653015,  speed:825326.13/s, info:group:49999, workers[2]:busy/n/min/max:7/21/20/512, chan:2499/32768, terminated:0, task:4608, cmdpost-chan-size:2141/16384, pushfail:0
//	grouptask_test.go:118: n:1763214,  speed:881214.90/s, info:group:49999, workers[2]:busy/n/min/max:3/21/20/512, chan:0/32768, terminated:0, task:2436, cmdpost-chan-size:2488/16384, pushfail:0
//	grouptask_test.go:118: n:1762432,  speed:877169.66/s, info:group:49999, workers[2]:busy/n/min/max:3/21/20/512, chan:9099/32768, terminated:0, task:9679, cmdpost-chan-size:580/16384, pushfail:0
//	grouptask_test.go:118: n:1758118,  speed:878880.06/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:2947, cmdpost-chan-size:3031/16384, pushfail:0
//	grouptask_test.go:118: n:1760131,  speed:879686.53/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:3469, cmdpost-chan-size:3593/16384, pushfail:0
//	grouptask_test.go:118: n:1629532,  speed:814644.17/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:7856, cmdpost-chan-size:8531/16384, pushfail:0
//	grouptask_test.go:118: n:1506009,  speed:752867.14/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:6880, cmdpost-chan-size:7453/16384, pushfail:0
//	grouptask_test.go:118: n:1505018,  speed:752173.15/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:5682, cmdpost-chan-size:6087/16384, pushfail:0
//	grouptask_test.go:118: n:1513598,  speed:756459.12/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:7530, cmdpost-chan-size:8147/16384, pushfail:0
//	grouptask_test.go:118: n:1524575,  speed:762179.31/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:5626, cmdpost-chan-size:5945/16384, pushfail:0
//	grouptask_test.go:118: n:1449450,  speed:724506.42/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:3649, cmdpost-chan-size:3802/16384, pushfail:0
//	grouptask_test.go:118: n:1458760,  speed:729045.55/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:4875, cmdpost-chan-size:5120/16384, pushfail:0
//	grouptask_test.go:118: n:1497387,  speed:748515.28/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:6621, cmdpost-chan-size:7081/16384, pushfail:0
//	grouptask_test.go:118: n:1493993,  speed:746990.37/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:395, cmdpost-chan-size:392/16384, pushfail:0
//	grouptask_test.go:118: n:1469544,  speed:734755.95/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:2620, cmdpost-chan-size:2689/16384, pushfail:0
//	grouptask_test.go:118: n:1500251,  speed:749992.94/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:4092, cmdpost-chan-size:4255/16384, pushfail:0
//	grouptask_test.go:118: n:1546383,  speed:773029.36/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:4184, cmdpost-chan-size:4340/16384, pushfail:0
//	grouptask_test.go:118: n:1501542,  speed:750730.16/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:3440, cmdpost-chan-size:3538/16384, pushfail:0
//	grouptask_test.go:118: n:1532704,  speed:766231.40/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:5846, cmdpost-chan-size:6192/16384, pushfail:0
//	grouptask_test.go:118: n:1524835,  speed:762310.32/s, info:group:49999, workers[2]:busy/n/min/max:3/21/20/512, chan:0/32768, terminated:0, task:2951, cmdpost-chan-size:3039/16384, pushfail:0
//	grouptask_test.go:118: n:1504928,  speed:752393.01/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:4527, cmdpost-chan-size:4771/16384, pushfail:0
//	grouptask_test.go:118: n:1522090,  speed:760755.65/s, info:group:49999, workers[2]:busy/n/min/max:3/21/20/512, chan:0/32768, terminated:0, task:5980, cmdpost-chan-size:6323/16384, pushfail:0
//	grouptask_test.go:118: n:1535763,  speed:767673.73/s, info:group:49999, workers[2]:busy/n/min/max:1/21/20/512, chan:0/32768, terminated:0, task:2121, cmdpost-chan-size:2162/16384, pushfail:0
//	grouptask_test.go:118: n:1525221,  speed:762366.28/s, info:group:49999, workers[2]:busy/n/min/max:0/21/20/512, chan:0/32768, terminated:0, task:3828, cmdpost-chan-size:3969/16384, pushfail:0
//	grouptask_test.go:118: n:1535660,  speed:767724.17/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:5371, cmdpost-chan-size:5650/16384, pushfail:0
//	grouptask_test.go:118: n:1521199,  speed:760471.32/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:3237, cmdpost-chan-size:3326/16384, pushfail:0
//	grouptask_test.go:118: n:1519116,  speed:759508.29/s, info:group:49999, workers[2]:busy/n/min/max:2/21/20/512, chan:0/32768, terminated:0, task:5425, cmdpost-chan-size:5661/16384, pushfail:0
//*/
//func TestMGSpeed(t *testing.T) {
//	group := make([]string, 50000)
//	for i := 0; i < len(group); i++ {
//		group[i] = gobase.RandHexString(128)
//	}
//	grouptask.DefaultGroupTask().Stop()
//	grouptask.DefaultGroupTask().ConfigMaxWork(512)
//	grouptask.DefaultGroupTask().Start()
//
//	n := int64(0)
//	go func() {
//		for {
//			idx := rand.Intn(len(group) - 1)
//			grouptask.DefaultGroupTask().PostTaskFunc(group[idx], func() {
//				atomic.AddInt64(&n, 1)
//			})
//			time.Sleep(0)
//		}
//	}()
//
//	go func() {
//		for {
//			idx := rand.Intn(len(group) - 1)
//			grouptask.DefaultGroupTask().PostTaskFunc(group[idx], func() {
//				atomic.AddInt64(&n, 1)
//			})
//			time.Sleep(0)
//		}
//	}()
//
//	for {
//		p := n
//		t1 := time.Now()
//		time.Sleep(time.Second * 2)
//		n1 := n - p
//		t.Logf("n:%d,  speed:%.2f/s, info:%s", n1, float64(n1)/time.Since(t1).Seconds(), grouptask.DefaultGroupTask().StatusSimpleString())
//		//pl.CheckReleaseWorker()
//	}
//}
