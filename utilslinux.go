//go:build linux
// +build linux

package gobase

import (
	"os"
	"syscall"
)

func ConfigIpAddress(interface_id string, ip, mask, gw string) {

}

func RedirectStderr(f *os.File, msg string) error {
	err := syscall.Dup3(int(f.Fd()), int(os.Stderr.Fd()), 0)
	if err != nil {
		// log.Fatalf("Failed to redirect stderr to file: %v, msg:%s", err, msg)
		return err
	}
	return nil
}

// disk usage of path/disk
func DiskUsage(path string) (disk DiskStatus, err error) {
	fs := syscall.Statfs_t{}
	err = syscall.Statfs(path, &fs)
	if err != nil {
		return
	}
	disk.All = fs.Blocks * uint64(fs.Bsize)
	disk.Free = fs.Bfree * uint64(fs.Bsize)
	disk.Used = disk.All - disk.Free
	return
}
