package quewriter_test

import (
	"gitee.com/ymofen/gobase"
	"gitee.com/ymofen/gobase/quewriter"
	"math/rand"
	"sync/atomic"
	"testing"
	"time"
)

func TestQueueWriter(t *testing.T) {
	var succN int32 = 0
	var runN int32 = 0
	buflst := make([][]byte, 1024)

	for i := 0; i < len(buflst); i++ {
		buflst[i] = gobase.RandBuf(rand.Intn(1024) + 12)
	}

	for i := 0; i < 1; i++ {
		go func(rundura time.Duration) {
			atomic.AddInt32(&runN, 1)
			que := quewriter.NewQueueWriter(quewriter.NewWriteWrapper(func(buf []byte) (n int, err error) {
				atomic.AddInt32(&succN, 1)
				return len(buf), nil
			}), 24)
			defer atomic.AddInt32(&runN, -1)
			time.Sleep(time.Millisecond)
			n := 0
			t0 := time.Now()
			for time.Since(t0) < rundura {
				buf := buflst[n%len(buflst)]
				_, err := que.Write(buf)
				if err != nil {
					break
				} else {
					time.Sleep(time.Millisecond * 10)
				}
				n++
			}
			que.Close()
			que.Wait()
		}(time.Second * time.Duration(rand.Intn(1)+30))
	}

	for {
		t0 := time.Now()
		atomic.StoreInt32(&succN, 0)
		time.Sleep(time.Second * 2)
		ms := time.Since(t0).Milliseconds()
		n := atomic.LoadInt32(&succN)
		t.Logf("runN:%d, %d, %d/s, stauts:%s", atomic.LoadInt32(&runN), n, int64(n)*1000/ms, quewriter.GetQueueWriterStatus())
	}
}
