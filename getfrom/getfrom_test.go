package getfrom

import (
	"github.com/stretchr/testify/require"
	"runtime"
	"testing"
)

func TestGetFrom(t *testing.T) {
	Register("file", func(args ...interface{}) (data []byte, err error) {
		return []byte(args[0].(string)), nil
	})

	buf, err := GetFromS("file://abc")
	require.NoError(t, err)
	require.Equal(t, "abc", string(buf))

	UnRegister("file")
	require.Equal(t, 0, len(defFactory.lst))
}

func TestGetFromInstance(t *testing.T) {
	Register("file", func(args ...interface{}) (data []byte, err error) {
		return []byte(args[0].(string)), nil
	})

	instance := NewGetFromInstance()
	_, arg, _ := instance.Update("file://abc")
	require.Equal(t, "abc", arg)

	{
		buf, err := instance.GetFrom()
		require.NoError(t, err)
		require.Equal(t, "abc", string(buf))
	}

	{
		buf, err := instance.GetFromArg("efg")
		require.NoError(t, err)
		require.Equal(t, "efg", string(buf))
	}

	{
		buf, err := instance.GetFromS("file://xyz")
		require.NoError(t, err)
		require.Equal(t, "xyz", string(buf))
	}

	instance.Close()

	runtime.Gosched()
	runtime.GC()
	runtime.Gosched()
	require.Zero(t, GetFromInstanceAliveN())

}
