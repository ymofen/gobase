package apputils

import (
	"gitee.com/ymofen/gobase"
	"sync"
)

var (
	// 程序开始时+1, 结束时-1, 可以用于插件中进行等待
	AppCloseWg    sync.WaitGroup
	AppInitFuncs  = gobase.NewSyncFuncList()
	AppFinalFuncs = gobase.NewSyncFuncList()
)
