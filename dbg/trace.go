package dbg

var NOOPPrintf = func(s string, args ...interface{}) {}
var NOOPPrintln = func(s string) {}

var Printf = NOOPPrintf
var Println = NOOPPrintln

func init() {

}
