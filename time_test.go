package gobase

import (
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestParseDUra(t *testing.T) {
	{
		s := "1"
		d := ParseDurationEx(s, 0)
		require.Equal(t, d, time.Second)
	}

	{
		s := "1s"
		d := ParseDurationEx(s, 0)
		require.Equal(t, d, time.Second)
	}

	{
		s := ""
		d := ParseDurationEx(s, time.Second)
		require.Equal(t, d, time.Second)
	}

	{
		s := "1d"
		d := ParseDurationEx(s, 0)
		require.Equal(t, d, time.Hour*24)
	}

}

func TestParseTime(t *testing.T) {
	t0 := TryStrToTime("2025-03-18 07:00:00", time.Local, ZeroTime)
	str := ParseTimeFmt("#utc:yyyy#-#utc:mm#-#utc:dd# #hh#:#mi# #utc:doy# #doy#", t0)
	t.Logf("%s", str)

}
