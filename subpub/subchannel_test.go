package subpub

import (
	"bytes"
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestSubchannelV2Chk(t *testing.T) {
	sub := NewSubchannel()
	var exec_n int32
	var fn001 = func(id, topic string, args ...interface{}) (ok bool) {
		atomic.AddInt32(&exec_n, 1)
		return true
	}

	if sub.Pub("topic/1", 0) != 0 {
		t.Fatal("pub err")
	}

	{
		sub.Sub("s02", "s01/s02", fn001)
		sub.Sub("s03", "s01/s02", fn001)
		if sub.Pub("s01/s02", 0) != 2 {
			t.Fatal("pub err")
		}
		sub.Unsub("s03", "s01/s02")
		if sub.Pub("s01/s02", 0) != 1 {
			t.Fatal("pub err")
		}
		sub.Unsub("s02", "s01/s02")
		if sub.Pub("s01/s02", 0) != 0 {
			t.Fatal("pub err")
		}
		exec_n = 0
	}

	{
		sub.Sub("s01", "s01/s02", fn001)
		sub.Sub("s01", "s01/s01", fn001)
		if sub.Pub("s01/s01", 0) != 1 {
			t.Fatal("pub err")
		}
		sub.Unsub("s01", "s01/s02")
		if sub.Pub("s01/s01", 0) != 1 {
			t.Fatal("pub err")
		}
		exec_n = 0
	}

	{
		sub.Sub("s02", "t02", fn001)
		sub.Sub("s02", "s02/t01", fn001)
		if sub.Pub("s02/s01", 0) != 0 {
			t.Fatal("pub err")
		}
		sub.Unsub("s02", "t02")
		if sub.Pub("s02/t01", 0) != 1 {
			t.Fatal("pub err")
		}
		exec_n = 0
	}

	{
		sub.Sub("s03", "s03/#", fn001)
		sub.Sub("s03", "s03/t01", fn001)
		if sub.Pub("s03/t01", 0) != 1 {
			t.Fatal("pub err")
		}
		sub.Unsub("s03", "s03/#")
		sub.Unsub("s03", "s03/t01")
		if sub.Pub("s03/t01", 0) != 0 {
			t.Fatal("pub err")
		}
		exec_n = 0
	}

}

// cpu:92%
// === RUN   TestSubchannelV2MG_0
//
//	Subchannel_test.go:199: workn:10000, 16018587, 5071(ms), 3158861/s
//	Subchannel_test.go:199: workn:10000, 39962408, 10098(ms), 3957457/s
//	Subchannel_test.go:199: workn:10000, 64334045, 15158(ms), 4244230/s
//	Subchannel_test.go:199: workn:10000, 88964465, 20165(ms), 4411825/s
//	Subchannel_test.go:199: workn:10000, 110095844, 25165(ms), 4374959/s
//	Subchannel_test.go:199: workn:10000, 132420223, 30166(ms), 4389717/s
//	Subchannel_test.go:199: workn:6445, 132425226, 35167(ms), 3765610/s
//	Subchannel_test.go:205: topic-n:0, sess:0, 132425226
//
// --- PASS: TestSubchannelV2MG_0 (40.16s)
func TestSubchannelV2MG_0(t *testing.T) {
	sub := NewSubchannel()
	var exec_n int32
	channel_max := 10000
	channels := make([]string, 0, 1024)
	databuf := make([][]byte, channel_max)
	for i := 0; i < channel_max; i++ {
		id := fmt.Sprintf("topic/%X/%X", i, channel_max+i)
		channels = append(channels, id)
		databuf[i] = RandBuf(rand.Intn(2048))
	}

	var runonce = func(id string, databuf []byte, dura time.Duration, channel string, beginfn, donefn func()) {
		t0 := time.Now()
		beginfn()
		sub.Sub(id, channel, func(id, topic string, args ...interface{}) (ok bool) {
			if bytes.Compare(args[0].([]byte), databuf) != 0 {
				t.Fatalf("recv err")
			}
			atomic.AddInt32(&exec_n, 1)
			return true
		})
		for time.Since(t0) < dura {
			sub.Pub(channel, 0, databuf, id, dura, channel)
			time.Sleep(0)
		}
		sub.Unsub(id, channel)
		donefn()
	}

	dura := time.Second * 30
	t0 := time.Now()

	var work_n int32 = 0
	var wt sync.WaitGroup
	for j := 0; j < 10000; j++ {
		wt.Add(1)
		go runonce(fmt.Sprintf("%X", j), databuf[j%len(databuf)], dura, channels[j%len(channels)], func() {
			atomic.AddInt32(&work_n, 1)
		}, func() {
			atomic.AddInt32(&work_n, -1)
			wt.Done()
		})
	}

	doneflag := 0

	go func() {
		for doneflag == 0 {
			time.Sleep(time.Second * 5)
			ms := time.Since(t0).Milliseconds()
			n := atomic.LoadInt32(&exec_n)
			t.Logf("workn:%d, %d, %d(ms), %d/s lk:%d\r\n", work_n, n, ms, int64(n)*1000/int64(ms), sub.lockcnt)
		}
	}()

	wt.Wait()
	doneflag = 1
	t.Logf("%s, %d", sub.Status(), exec_n)
}

// cpu:95%
// Subchannel_test.go:255: 91179, 18/ms, lk:24101
// Subchannel_test.go:255: 75863422, 7556/ms, lk:26467
// Subchannel_test.go:255: 159122613, 10570/ms, lk:26467
// Subchannel_test.go:255: 242062908, 12065/ms, lk:26467
// Subchannel_test.go:255: 324191541, 12921/ms, lk:26467
// Subchannel_test.go:265: topic-n:10000, sess:10000, 389789819, 389789819
func TestSubchannelV2MG(t *testing.T) {
	sub := NewSubchannel()
	var exec_n int32
	var pub_n int32
	channel_max := 10000
	channellst := make([]string, channel_max)
	var wt sync.WaitGroup
	// 订阅N个通道数据
	for i := 0; i < channel_max; i++ {
		id := fmt.Sprintf("id-%X", i%channel_max)
		channel := fmt.Sprintf("topic/%X", i%channel_max)
		channellst[i] = channel
		sub.Sub(id, channel, func(id, topic string, args ...interface{}) (ok bool) {
			atomic.AddInt32(&exec_n, 1)
			return true
		})

		wt.Add(1)
		time.AfterFunc(time.Second*30, func() {
			sub.Unsub(id, channel)
			sub.ClosePubChannel(channel)
			wt.Done()
		})
		//sub.Pub(channel, 0)
	}

	t0 := time.Now()
	dura := time.Second * 30

	for j := 0; j < 10; j++ {
		wt.Add(1)
		go func() {
			idx := 0
			for time.Since(t0) < dura {
				channelsn := idx % channel_max
				if channelsn == 0 {
					channelsn = 0
				}

				channel := channellst[channelsn]
				n := sub.Pub(channel, 0)
				atomic.AddInt32(&pub_n, int32(n))
				//time.Sleep(0)
				idx++
			}
			wt.Done()
		}()
	}

	doneflag := 0

	go func() {
		for doneflag == 0 {
			time.Sleep(time.Second * 1)
			n0 := atomic.LoadInt32(&exec_n)
			t.Logf("%d, %d/ms, status:%s, lk:%d\r\n", n0, int64(n0)/time.Since(t0).Milliseconds(), sub.Status(), sub.lockcnt)
			sub.CleanChannels()
		}
	}()

	wt.Wait()
	doneflag = 1
	if exec_n != pub_n {
		t.Fatalf("err")
	}

	t.Logf("%s, %d, %d", sub.Status(), exec_n, pub_n)

}

func RandBuf(len int) []byte {
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		b := rand.Intn(255)
		bytes[i] = byte(b)
	}
	return bytes
}
