package bypass

import (
	"gitee.com/ymofen/gobase"
	"github.com/stretchr/testify/require"
	"math/rand"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestByPassStratgy(t *testing.T) {
	byPass := NewByPassManager()

	var reloadConf = func() {
		{
			byPassGroup := byPass.CheckGetByPassGroup("r11", true)
			byPassGroup.BeginUpdate()
			byPassGroup.ConfigStrategy(2)
			byPassGroup.UpdateByPassNode("r11_0", 0, rand.Intn(20000), "connstr=r11_0")
			byPassGroup.UpdateByPassNode("r11_1", 1, rand.Intn(20000), "connstr=r11_1")
			byPassGroup.EndUpdate(true)
		}

		{
			byPassGroup := byPass.CheckGetByPassGroup("r12", true)
			byPassGroup.BeginUpdate()
			byPassGroup.ConfigStrategy(0)
			byPassGroup.UpdateByPassNode("r12_0", 0, rand.Intn(1000), "connstr=r12_0")
			byPassGroup.UpdateByPassNode("r12_1", 1, rand.Intn(20000), "connstr=r12_1")
			byPassGroup.EndUpdate(true)
		}
		byPass.TrySetDefault("rxx")
	}

	reloadConf()

	var wg sync.WaitGroup
	{
		node := byPass.GetByPassNode("r11")
		require.NotNil(t, node)
		require.Equal(t, "r11_0", node.id)
		wg.Add(1)
		time.AfterFunc(time.Second, func() {
			byPass.ReleaseByPassNode(node, false, 0, "")
			wg.Done()
		})
	}

	{
		node := byPass.GetByPassNode("r11")
		require.NotNil(t, node)
		require.Equal(t, "r11_1", node.id)
		wg.Add(1)
		time.AfterFunc(time.Second, func() {
			byPass.ReleaseByPassNode(node, false, 0, "")
			wg.Done()
		})
	}

	wg.Wait()

	t.Logf(byPass.GetStatusText(0))

}

func TestByPassManangerCheckStrategy(t *testing.T) {
	byPass := NewByPassManager()

	var reloadConf = func() {
		{
			byPassGroup := byPass.CheckGetByPassGroup("r11", true)
			byPassGroup.BeginUpdate()
			byPassGroup.ConfigStrategy(2)
			byPassGroup.UpdateByPassNode("r11_0", 0, 4000, "connstr=r11_0")
			byPassGroup.UpdateByPassNode("r11_1", 1, 0, "connstr=r11_1")
			byPassGroup.EndUpdate(true)
		}
	}

	reloadConf()

	go func() {
		for {
			time.Sleep(time.Second * time.Duration(rand.Intn(10)))
			reloadConf()
		}
	}()

	var succ_n int32 = 0
	var fail_n int32 = 0

	start0 := time.Now()

	var logstatus = func(prefix string) {
		ms := time.Since(start0).Milliseconds()
		t.Logf("%ssucc:%d, fail:%d, speed:%d/ms, status:\n%s", prefix, succ_n, fail_n, int64(succ_n)/ms, byPass.GetStatusText(0))
	}

	var runOnce = func(groupid string, d time.Duration, donefn func()) {
		defer donefn()
		time.Sleep(time.Millisecond * time.Duration(rand.Intn(5000)))
		t0 := time.Now()
		for time.Since(t0) < d {
			itm := byPass.GetByPassNode(groupid)
			if itm == nil {
				atomic.AddInt32(&fail_n, 1)
			} else {
				atomic.AddInt32(&succ_n, 1)
				// 				time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
				time.Sleep(time.Millisecond)
				byPass.ReleaseByPassNode(itm, false, 0, "")
			}
			time.Sleep(time.Millisecond)
		}
	}

	go func() {
		for {
			time.Sleep(time.Second * 5)
			logstatus("")
		}
	}()

	var wg sync.WaitGroup
	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go runOnce("r11", time.Second*time.Duration(rand.Intn(60)), func() {
			wg.Done()
		})
	}

	wg.Wait()

	byPassGroup := byPass.CheckGetByPassGroup("r11", true)

	s := byPassGroup.OnlineStatus()
	attrMap := gobase.NewStrMap()
	attrMap.URLFormDecode(s)
	t.Logf("%s", s)

	s1, _ := gobase.Split2Str(attrMap["r11_0"], "/")

	t.Logf("%s", s1)

	require.Equal(t, "0", s1)

	logstatus("===========\n")
}
