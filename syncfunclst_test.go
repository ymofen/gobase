package gobase

import (
	"github.com/stretchr/testify/require"
	"runtime"
	"sync/atomic"
	"testing"
	"time"
)

func TestSyncFuncList(t *testing.T) {
	statuslst := GroupFuncsCheckGet("001", true)
	statuslst.AddOrSet("001", func(args ...interface{}) {
		t.Logf("%v", args[0])
	}, "00001")

	statuslst.AddOrSet("002", func(args ...interface{}) {
		t.Logf("%v", args[0])
		statuslst.Remove("002")
	}, "00002")

	require.True(t, statuslst.Execute("001"))
	require.Equal(t, 2, statuslst.ExecuteFuncs())
	require.False(t, statuslst.Execute("002"))

	GroupFuncsClose("001")
	require.Equal(t, 0, len(gMap))
}

type StoreObject struct {
	key   string
	value int
}

func TestSyncList2(t *testing.T) {
	var N int32
	obj := &StoreObject{}
	atomic.AddInt32(&N, 1)
	runtime.SetFinalizer(obj, func(obj interface{}) {
		atomic.AddInt32(&N, -1)
	})
	obj.key = "HB01"

	statuslst := NewSyncFuncList()

	n0 := 0
	statuslst.AddOrSet(obj, func(args ...interface{}) {
		t.Logf("%v", args[0])
		n0++
	}, "init")

	statuslst.ExecuteWithArgsAndRemove(obj, "000")

	statuslst.AddOrSet(obj, func(args ...interface{}) {
		t.Logf("%v", args[0])
		n0++
	}, "init")
	statuslst.ExecuteAndRemove(obj)
	require.Equal(t, 2, n0)

	n0 = 0

	statuslst.AddOrSet(obj, func(args ...interface{}) {
		t.Logf("%v", args[0])
	}, "00001")

	statuslst.AddOrSet("002", func(args ...interface{}) {
		t.Logf("%v", args[0])
		if args[0] == "00002" {
			statuslst.Remove("002")
		}
	}, "00002")

	n := 0
	for i := 0; i < 1000; i++ {
		statuslst.AddOrSet(RandKeyString(12), func(args ...interface{}) {
			n++
		}, "00002")
	}

	runtime.GC()
	time.Sleep(time.Second)
	require.Equal(t, 1, int(N))

	statuslst.ExecuteWithArgs(obj, "abc")
	require.Equal(t, 2+1000, statuslst.ExecuteAllWithArgs("abcef"))

	n = 0
	require.True(t, statuslst.Execute(obj))
	require.Equal(t, 2+1000, statuslst.ExecuteAndRemoveAll())
	require.Equal(t, 1000, n)
	require.False(t, statuslst.Execute("002"))

	//obj = nil

	runtime.GC()
	time.Sleep(time.Second)

	require.Equal(t, 0, len(gMap))
	require.Equal(t, 0, int(N))
}
