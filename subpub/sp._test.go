package subpub

import "testing"

func TestSubpubUsage(t *testing.T) {
	{
		sub := NewSubscribe()
		sub.Sub("001", "gps/001/cmd", func(id, topic string, args ...interface{}) (ok bool) {
			return true
		})

		r := sub.SubR("002", "gps/001/cmd", func(id, topic string, args ...interface{}) (ok bool) {
			return true
		})

		if r != 2 {
			t.Errorf("r must be 2")
		}

		r = sub.UnsubR("002", "gps/001/cmd")
		if r != 1 {
			t.Errorf("r must be 1")
		}
	}

	{
		sub := NewSubchannel()
		sub.Sub("001", "gps/001/cmd", func(id, topic string, args ...interface{}) (ok bool) {
			return true
		})

		var obj interface{} = sub
		subI := obj.(ISubRA)

		r := subI.SubR("002", "gps/001/cmd", func(id, topic string, args ...interface{}) (ok bool) {
			return true
		})

		if r != 2 {
			t.Errorf("r must be 2")
		}

		r = sub.UnsubR("002", "gps/001/cmd")
		if r != 1 {
			t.Errorf("r must be 1")
		}
	}

}
