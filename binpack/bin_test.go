package binpack_test

import (
	"gitee.com/ymofen/gobase"
	"gitee.com/ymofen/gobase/binpack"
	"github.com/stretchr/testify/require"
	"math/rand"
	"testing"
)

func TestBinPackL(t *testing.T) {
	lmax := 0x3FFF
	for n := 0; n < lmax; n++ {
		l0 := n
		l1 := 0x80 | ((l0 & 0x3F80) >> 7) // 高7位
		l2 := 0x80 | (l0 & 0x007F)        // 低7位
		//require.Equal(t, l1, l2, "%d != %d", l1, l2)

		require.Equal(t, int(0x80), l1&0x80)
		require.Equal(t, int(0x80), l2&0x80)

		l3 := (l1&0x7F)<<7 + (l2 & 0x7F)
		require.Equal(t, int(l0), int(l3), "%d != %d", int(l0), int(l3))
	}
}

func TestBinPackStr(t *testing.T) {
	var bb gobase.BytesBuilder
	key0 := ""
	var data0 = []byte(nil)
	binpack.WriteBinPackStr(&bb, key0, data0)
	packBuf := bb.Bytes()

	var dec binpack.BinPackStrDecode
	var r int

	for i := 0; i < len(packBuf); i++ {
		r = dec.InputByte(packBuf[i])
	}
	require.Equal(t, 1, r)
	key1, data1 := dec.ExtractMsg()

	require.Equal(t, key0, key1)
	require.Equal(t, data0, data1)

}

// BenchmarkBinPackStr-8   	    2563	    461982 ns/op
// BenchmarkBinPackStr-8   	    2491	    458576 ns/op
func BenchmarkBinPackStr(b *testing.B) {
	buflst := make([][]byte, 1024)
	keylst := make([]string, 1024)
	packlst := make([][]byte, 1024)
	for j := 0; j < len(buflst); j++ {
		buflst[j] = gobase.RandBuf(rand.Intn(0xFFFF))
		keylst[j] = gobase.RandKeyString(rand.Intn(128))
		var bb gobase.BytesBuilder
		bodybuf := buflst[j]
		msgid0 := keylst[j]
		binpack.WriteBinPackStr(&bb, msgid0, bodybuf)
		packlst[j] = bb.Bytes()
	}

	var bb gobase.BytesBuilder
	var packFunc = func(idx int) {
		bb.Cleanup()
		bodybuf := buflst[idx%len(buflst)]
		msgid0 := keylst[idx%len(buflst)]
		binpack.WriteBinPackStr(&bb, msgid0, bodybuf)
	}

	var unpackFunc = func(idx int) {
		bodybuf := buflst[idx%len(buflst)]
		msgid0 := keylst[idx%len(buflst)]
		packBuf := packlst[idx%len(buflst)]

		var dec binpack.BinPackStrDecode
		var r int
		for i := 0; i < len(packBuf); i++ {
			r = dec.InputByte(packBuf[i])
		}
		require.Equal(b, 1, r)
		msgid, body := dec.ExtractMsg()
		require.Equal(b, msgid0, msgid)
		require.Equal(b, bodybuf, body)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		packFunc(i)
		unpackFunc(i)
	}
}

// BenchmarkBinPack-8   	    2397	    461258 ns/op
// BenchmarkBinPack-8   	    2416	    475906 ns/op
// BenchmarkBinPack-8   	    2356	    483951 ns/op
func BenchmarkBinPack(b *testing.B) {
	buflst := make([][]byte, 1024)
	msglst := make([]uint16, 1024)
	packlst := make([][]byte, 1024)
	for j := 0; j < len(buflst); j++ {
		buflst[j] = gobase.RandBuf(rand.Intn(0xFFFF))
		msglst[j] = uint16(rand.Intn(0xFFF))
		var bb gobase.BytesBuilder
		bodybuf := buflst[j]
		msgid0 := msglst[j]
		binpack.WriteBinPack(&bb, uint16(msgid0), bodybuf)
		packlst[j] = bb.Bytes()
	}

	var bb gobase.BytesBuilder
	var packFunc = func(idx int) {
		bb.Cleanup()
		bodybuf := buflst[idx%len(buflst)]
		msgid0 := msglst[idx%len(buflst)]
		binpack.WriteBinPack(&bb, uint16(msgid0), bodybuf)
	}

	var unpackFunc = func(idx int) {
		bodybuf := buflst[idx%len(buflst)]
		msgid0 := msglst[idx%len(buflst)]
		packBuf := packlst[idx%len(buflst)]

		var dec binpack.BinPackDecode
		var r int
		for i := 0; i < len(packBuf); i++ {
			r = dec.InputByte(packBuf[i])
		}
		require.Equal(b, 1, r)
		msgid, body := dec.ExtractMsg()
		require.Equal(b, uint16(msgid0), msgid)
		require.Equal(b, bodybuf, body)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		packFunc(i)
		unpackFunc(i)
	}
}

func BenchmarkBinUnPack(b *testing.B) {
	buflst := make([][]byte, 1024)
	msglst := make([]uint16, 1024)
	packlst := make([][]byte, 1024)
	for j := 0; j < len(buflst); j++ {
		//buflst[j] = gobase.RandBuf(rand.Intn(0xFFFF))
		buflst[j] = gobase.RandBuf(2048)
		msglst[j] = uint16(rand.Intn(0xFFF))
		var bb gobase.BytesBuilder
		bodybuf := buflst[j]
		msgid0 := msglst[j]
		binpack.WriteBinPack(&bb, uint16(msgid0), bodybuf)
		packlst[j] = bb.Bytes()
	}

	var unpackFunc = func(idx int) {
		bodybuf := buflst[idx%len(buflst)]
		msgid0 := msglst[idx%len(buflst)]
		packBuf := packlst[idx%len(buflst)]

		var dec binpack.BinPackDecode
		var r int
		for i := 0; i < len(packBuf); i++ {
			r = dec.InputByte(packBuf[i])
		}
		require.Equal(b, 1, r)
		msgid, body := dec.ExtractMsg()
		require.Equal(b, uint16(msgid0), msgid)
		require.Equal(b, bodybuf, body)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		unpackFunc(i)
	}
}

func BenchmarkBinPackBuf(b *testing.B) {
	buflst := make([][]byte, 1024)
	msglst := make([]uint16, 1024)
	packlst := make([][]byte, 1024)
	for j := 0; j < len(buflst); j++ {
		buflst[j] = gobase.RandBuf(2048)
		//buflst[j] = gobase.RandBuf(rand.Intn(0xFFFF))
		msglst[j] = uint16(rand.Intn(0xFFF))
		var bb gobase.BytesBuilder
		bodybuf := buflst[j]
		msgid0 := msglst[j]
		binpack.WriteBinPack(&bb, uint16(msgid0), bodybuf)
		packlst[j] = bb.Bytes()
	}

	var bb gobase.BytesBuilder
	var packFunc = func(idx int) {
		bb.Cleanup()
		bodybuf := buflst[idx%len(buflst)]
		msgid0 := msglst[idx%len(buflst)]
		binpack.WriteBinPack(&bb, uint16(msgid0), bodybuf)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		packFunc(i)
	}
}

func TestBinPack(t *testing.T) {
	buflst := make([][]byte, 1024)
	msglst := make([]uint16, 1024)
	for j := 0; j < len(buflst); j++ {
		buflst[j] = gobase.RandBuf(rand.Intn(0xFFFF))
		msglst[j] = uint16(rand.Intn(0xFFF))
	}

	for i := 0; i < 10000; i++ {
		var bb gobase.BytesBuilder
		bodybuf := buflst[i%len(buflst)]
		msgid0 := msglst[i%len(buflst)]
		binpack.WriteBinPack(&bb, uint16(msgid0), bodybuf)
		var dec binpack.BinPackDecode
		var r int
		for i := 0; i < bb.Len(); i++ {
			r = dec.InputByte(bb.Byte(i))
		}

		require.Equal(t, 1, r)
		msgid, body := dec.ExtractMsg()
		require.Equal(t, uint16(msgid0), msgid)
		require.Equal(t, bodybuf, body)
	}
}
