package gobase

import "math"

func Cfmod(x, y float64) float64 {
	_, r := math.Modf(x / y)
	return r * y
}

func AbsInt(x int) int {
	if x < 0 {
		return -x
	} else {
		return x
	}
}

func MinInt(a, b int) int {
	if a > b {
		return b
	} else {
		return a
	}
}

func Round32(f float32, n int) float32 {
	p := math.Pow10(n)
	return float32(math.Round(float64(f)*p) / p)
}

func Round64(f float64, n int) float64 {
	p := math.Pow10(n)
	return math.Round(f*p) / p
}

/*
*
  - 取10进制的 各个位上的值
  - n <=1 为个位, 2:十位 ....
  - GetDecimalBitN(12304, 1) = 4,
    % 取余数
*/
func GetDecimalBitN(v int64, n int64) int {
	if n <= 1 {
		r := v % 10
		return int(r)
	} else {
		// 取百位(3) nPow = 1000, n2 := 100
		nPow := int64(math.Pow(10, float64(n)))
		n2 := int64(math.Pow(10, float64(n-1)))
		r := v % nPow // 去掉 多余的数据 比如 12345 :   取百位 = 235
		r = r / n2
		return int(r)
	}
}

/**
 * 取10进制的 各个位上的值
 *  n 1,10,1000, 10000 ..
 * GetDecimalBitNEx(12304, 1) = 4,
 */
func GetDecimalBitNEx(v int64, n int64) int {
	if n <= 1 {
		r := v % 10
		return int(r)
	} else {
		// 取百位(3) nPow = 1000, n2 := 100
		nPow := n * 10
		r := v % nPow // 去掉 多余的数据 比如 12345 :   取百位 = 235
		r = r / n
		return int(r)
	}
}
