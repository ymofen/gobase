package factory

import "gitee.com/ymofen/gobase"

var (
	// Deprecated:
	RegisterCreateFunc   = gobase.RegisterFactoryCreateFunc
	UnregisterCreateFunc = gobase.UnregisterFactoryCreateFunc
	CreateInstance       = gobase.CreateFactoryInstance
	RegisterVar          = gobase.RegisterFactoryVar
	GetVar               = gobase.GetFactoryVar
	RemoveVar            = gobase.RemoveFactoryVar
	RangeVarList         = gobase.RangeFactoryVarList
	RangeVarListEx       = gobase.RangeFactoryVarListEx
)

//
//type t_createfun func(args ...interface{}) (rval interface{}, err error)
//
//type varFactory struct {
//	lock      *sync.RWMutex
//	varMap    map[string]interface{}
//	createMap map[string]t_createfun
//}
//
//var (
//	defFactory *varFactory
//)
//
//func init() {
//	defFactory = newVarFactory()
//}
//
//func newVarFactory() *varFactory {
//	rval := &varFactory{
//		lock:      new(sync.RWMutex),
//		varMap:    make(map[string]interface{}),
//		createMap: make(map[string]t_createfun),
//	}
//	return rval
//}
//
//// 注册一个创建方法函数
//func RegisterCreateFunc(id string, cfun t_createfun) error {
//	defFactory.lock.Lock()
//	defer defFactory.lock.Unlock()
//	if defFactory.createMap[id] != nil {
//		fmt.Fprintf(os.Stderr, "%s is registed\n", id)
//		return errors.New(id + " is registed!")
//	}
//	defFactory.createMap[id] = cfun
//	return nil
//}
//
//// 移除一个创建方法
//func UnregisterCreateFunc(id string) {
//	defFactory.lock.Lock()
//	defer defFactory.lock.Unlock()
//	delete(defFactory.createMap, id)
//	return
//}
//
//// 使用方法创建一个对象
//func CreateInstance(id string, args ...interface{}) (rval interface{}, err error) {
//	defFactory.lock.RLock()
//	defer defFactory.lock.RUnlock()
//	f := defFactory.createMap[id]
//	if f == nil {
//		return nil, fmt.Errorf("[%s]未注册创建方法", id)
//	}
//
//	return f(args...)
//}
//
//// 获取一个全局对象
//func GetVar(id string) interface{} {
//	defFactory.lock.RLock()
//	defer defFactory.lock.RUnlock()
//	return defFactory.varMap[id]
//}
//
//// 注册一个全局对象
//func RegisterVar(id string, val interface{}) {
//	defFactory.lock.Lock()
//	defer defFactory.lock.Unlock()
//	defFactory.varMap[id] = val
//}
//
//// 移除一个全局对象
//func RemoveFactoryVar(id string) {
//	defFactory.lock.Lock()
//	defer defFactory.lock.Unlock()
//	delete(defFactory.varMap, id)
//}
//
//// 循环所有的全局对象
////
////	cb 返回false 则终止循环
//func RangeFactoryVarList(cb func(val interface{}) bool) {
//	defFactory.lock.RLock()
//	defer defFactory.lock.RUnlock()
//	for _, itm := range defFactory.varMap {
//		if cb(itm) {
//			break
//		}
//	}
//}
//
//func RangeFactoryVarListEx(cb func(key string, val interface{}) bool) {
//	defFactory.lock.RLock()
//	defer defFactory.lock.RUnlock()
//	for key, val := range defFactory.varMap {
//		if cb(key, val) {
//			break
//		}
//	}
//}
