package chantask

import (
	"fmt"
	"gitee.com/ymofen/gobase"
	"sync"
	"sync/atomic"
	"time"
)

type chanTaskItem struct {
	fn   func(args ...interface{})
	args []interface{}
}

type ChanTask struct {
	refcnt      int32
	pushcnt     int32
	popcnt      int32
	pushfailcnt int32
	timeout     time.Duration
	wg          sync.WaitGroup
	terminated  int8
	taskChan    chan *chanTaskItem
}

func NewChanTask(maxCache int) *ChanTask {
	rval := &ChanTask{
		taskChan: make(chan *chanTaskItem, maxCache),
		timeout:  time.Second,
	}
	rval.start()
	return rval
}

func (this *ChanTask) IsTerminated() bool {
	return this.terminated == 1
}

func (this *ChanTask) innerExec(itm *chanTaskItem) {
	if gobase.GoFunCatchException {
		defer gobase.DeferCatchPanic()
	}
	itm.fn(itm.args...)
}

func (this *ChanTask) innerWork() {
	if gobase.GoFunCatchException {
		defer gobase.DeferCatchPanic()
	}
	defer this.wg.Done()
break_for:
	for {
		select {
		case itm := <-this.taskChan:
			if itm == nil {
				break break_for
			}
			atomic.AddInt32(&this.popcnt, 1)
			this.innerExec(itm)
		}
	}
}

func (this *ChanTask) Remain() int32 {
	return atomic.LoadInt32(&this.pushcnt) - atomic.LoadInt32(&this.popcnt)
}

func (this *ChanTask) start() {
	this.wg.Add(1)
	go this.innerWork()
}

func (this *ChanTask) Stop() {
	this.terminated = 1

	select {
	case this.taskChan <- nil:
	case <-time.After(time.Second * 3):
		close(this.taskChan)
	}

	this.wg.Wait()
}

func (this *ChanTask) StatusString() string {
	cnt1, cnt2 := atomic.LoadInt32(&this.pushcnt), atomic.LoadInt32(&this.popcnt)
	return fmt.Sprintf("%d-%d=%d, fail:%d", cnt1, cnt2, cnt1-cnt2, this.pushfailcnt)
}

func (this *ChanTask) PushTask(fn func(args ...interface{}), args ...interface{}) error {
	itm := &chanTaskItem{fn: fn, args: args}
	select {
	case this.taskChan <- itm:
		atomic.AddInt32(&this.pushcnt, 1)
	case <-time.After(this.timeout):
		atomic.AddInt32(&this.pushfailcnt, 1)
		return fmt.Errorf("ChanTask->PushTask timeout")
	}
	return nil
}
