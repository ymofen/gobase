package gobase

import "io"

func TryClose(v interface{}) (ok bool, err error) {
	if intf, ok0 := v.(io.Closer); ok0 {
		err = intf.Close()
		ok = true
		return
	} else {
		return false, ErrUnsupported
	}
}

func TryGetStatus(v interface{}, args ...interface{}) string {
	if intf, ok0 := v.(StatuGetter); ok0 {
		return intf.GetStatus(args...)
	} else {
		return ""
	}
}
