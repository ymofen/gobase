package golog

type Logger interface {
	Debug(s string)
	Info(s string)
	Warn(s string)
	Error(s string)
	Infof(s string, args ...interface{})
	Errorf(s string, args ...interface{})
	Debugf(s string, args ...interface{})
	Warnf(s string, args ...interface{})
}
