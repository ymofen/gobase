package gobase_test

import (
	"gitee.com/ymofen/gobase"
	"testing"
)

func TestBytesBuilder(t *testing.T) {
	var bb gobase.BytesBuilder
	bb.AppendStr(".00000=.00001=")
	idx := bb.IndexByte(7, '=')
	t.Logf("idx:%d", idx)
	if idx == -1 {
		return
	}
	bb.DeleteStart2End(0, idx)
	t.Logf("%s", bb.String())

}
