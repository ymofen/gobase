package subpub

import (
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestSubscribeV2Chk(t *testing.T) {
	sub := NewSubscribe()
	var exec_n int32
	var fn001 = func(id, topic string, args ...interface{}) (ok bool) {
		atomic.AddInt32(&exec_n, 1)
		return true
	}

	if sub.Pub("topic/1", 0) != 0 {
		t.Fatal("pub err")
	}

	{
		sub.Sub("s01", "s01/#", fn001)
		sub.Sub("s01", "s01/s01", fn001)
		if sub.Pub("s01/s01", 0) != 1 {
			t.Fatal("pub err")
		}
		sub.Unsub("s01", "s01/s01")
		if sub.Pub("s01/s01", 0) != 1 {
			t.Fatal("pub err")
		}
		exec_n = 0
	}

	{
		sub.Sub("s02", "s02/#", fn001)
		sub.Sub("s02", "s02/t01", fn001)
		if sub.Pub("s02/s01", 0) != 1 {
			t.Fatal("pub err")
		}
		sub.Unsub("s02", "s02/#")
		if sub.Pub("s02/t01", 0) != 1 {
			t.Fatal("pub err")
		}
		exec_n = 0
	}

	{
		sub.Sub("s03", "s03/#", fn001)
		sub.Sub("s03", "s03/t01", fn001)
		if sub.Pub("s03/t01", 0) != 1 {
			t.Fatal("pub err")
		}
		sub.Unsub("s03", "s03/#")
		sub.Unsub("s03", "s03/t01")
		if sub.Pub("s03/t01", 0) != 0 {
			t.Fatal("pub err")
		}
		exec_n = 0
	}

	sub.Sub("001", "topic/+/key", fn001)
	sub.Sub("002", "topic/news", func(id, topic string, args ...interface{}) (ok bool) {
		atomic.AddInt32(&exec_n, 1)
		return true
	})

	n := int32(100)
	for i := int32(0); i < n; i++ {
		if sub.Pub(fmt.Sprintf("topic/%d/key", i), 0) != 1 {
			t.Fatal("pub err")
		}
	}

	if exec_n != n {
		t.Fatalf("err %d", exec_n)
	}

	if sub.Pub("topic/news", 0) != 1 {
		t.Fatal("pub err")
	}
	sub.ClosePubChannel("topic/news")

	sub.Sub("001", "topic/a/key", fn001)
	if sub.Pub("topic/a/key", 0, 1, 2, 3) != 1 {
		t.Fatalf("pub err")
	}

	sub.Sub("002", "topic/a/key", fn001)
	if sub.Pub("topic/a/key", 0, 1, 2, 3) != 2 {
		t.Fatalf("pub err")
	}
	sub.ClosePubChannel("topic/a/key")

	if sub.Pub("topic/a/key", 1, 1, 2, 3) != 1 {
		t.Fatalf("pub err")
	}

	if sub.Pub("topic/b/key", 0, 1, 2, 3) != 1 {
		t.Fatalf("pub err")
	}
}

// cpu:95%
// subscribe_test.go:255: 91179, 18/ms, lk:24101
// subscribe_test.go:255: 75863422, 7556/ms, lk:26467
// subscribe_test.go:255: 159122613, 10570/ms, lk:26467
// subscribe_test.go:255: 242062908, 12065/ms, lk:26467
// subscribe_test.go:255: 324191541, 12921/ms, lk:26467
// subscribe_test.go:265: topic-n:10000, sess:10000, 389789819, 389789819
func TestSubscribeV2MG(t *testing.T) {
	sub := NewSubscribe()
	var exec_n int32
	var pub_n int32
	channel_max := 10000
	channellst := make([]string, channel_max)
	var wt sync.WaitGroup
	// 订阅N个通道数据
	for i := 0; i < channel_max; i++ {
		id := fmt.Sprintf("id-%X", i%channel_max)
		channel := fmt.Sprintf("topic/%X", i%channel_max)
		channellst[i] = channel
		sub.Sub(id, channel, func(id, topic string, args ...interface{}) (ok bool) {
			atomic.AddInt32(&exec_n, 1)
			return true
		})

		wt.Add(1)
		time.AfterFunc(time.Second*30, func() {
			sub.Unsub(id, channel)
			sub.ClosePubChannel(channel)
			wt.Done()
		})
		//sub.Pub(channel, 0)
	}

	t0 := time.Now()
	dura := time.Second * 30

	for j := 0; j < 10; j++ {
		wt.Add(1)
		go func() {
			idx := 0
			for time.Since(t0) < dura {
				channelsn := idx % channel_max
				if channelsn == 0 {
					channelsn = 0
				}

				channel := channellst[channelsn]
				n := sub.Pub(channel, 0)
				atomic.AddInt32(&pub_n, int32(n))
				//time.Sleep(0)
				idx++
			}
			wt.Done()
		}()
	}

	doneflag := 0

	go func() {
		for doneflag == 0 {
			time.Sleep(time.Second * 1)
			n0 := atomic.LoadInt32(&exec_n)
			t.Logf("%d, %d/ms, status:%s, lk:%d\r\n", n0, int64(n0)/time.Since(t0).Milliseconds(), sub.Status(), sub.lockcnt)
			sub.CleanChannels()
		}
	}()

	wt.Wait()
	doneflag = 1
	if exec_n != pub_n {
		t.Fatalf("err")
	}

	t.Logf("%s, %d, %d", sub.Status(), exec_n, pub_n)

}

func TestSubscribeUnsub(t *testing.T) {
	sub := NewSubscribe()
	var exec_n int32
	channel_max := 10000
	channellst := make([]string, channel_max)

	t0 := time.Now()
	// 订阅N个通道数据
	for i := 0; i < channel_max; i++ {
		id := fmt.Sprintf("id-%X", i%channel_max)
		channel := fmt.Sprintf("topic/%X", i%channel_max)
		channellst[i] = channel
		sub.Sub(id, channel, func(id, topic string, args ...interface{}) (ok bool) {
			atomic.AddInt32(&exec_n, 1)
			return true
		})
	}
	t.Logf("sub:%dms, %s", time.Since(t0).Milliseconds(), sub.Status())

	go func() {
		for {
			t.Logf("%s, exec:%d", sub.Status(), exec_n)
			time.Sleep(time.Second * 2)
		}
	}()

	go func() {
		//t0 := time.Now()
		for {
			for i := 0; i < channel_max; i++ {
				id := fmt.Sprintf("id-%X", i%channel_max)
				channel := fmt.Sprintf("topic/%X", i%channel_max)
				channellst[i] = channel
				sub.Pub(channel, 0, id)
				//time.Sleep(0)
			}
			//time.Sleep(0)
		}

		//t.Logf("pub:%dms, %s", time.Since(t0).Milliseconds(), sub.Status())
	}()

	time.Sleep(time.Second * 5)

	go func() {
		t0 := time.Now()
		var wg sync.WaitGroup
		for i := 0; i < channel_max; i++ {
			id := fmt.Sprintf("id-%X", i%channel_max)
			channel := fmt.Sprintf("topic/%X", i%channel_max)
			channellst[i] = channel
			wg.Add(1)
			go func() {
				sub.Unsub(id, channel)
				wg.Done()
			}()
		}
		wg.Wait()
		t.Logf("unsub:%dms, %s", time.Since(t0).Milliseconds(), sub.Status())
	}()

	time.Sleep(time.Second * 60)

}
