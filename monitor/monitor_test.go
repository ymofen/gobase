package monitor

import (
	"context"
	"github.com/stretchr/testify/require"
	"math/rand"
	"runtime"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestMonitor(t *testing.T) {
	//SetIntervalEngineer(NewTickerIntervalEnginner(time.Millisecond * 1500))

	{
		m := NewMonitorSpeedSize()

		var wg sync.WaitGroup
		wg.Add(1)
		ctx, cancel := context.WithCancel(context.Background())
		go func() {
			defer wg.Done()
			for {
				select {
				case <-ctx.Done():
					return
				default:
					m.Delta(int64(rand.Intn(512)))
					time.Sleep(time.Millisecond * time.Duration(rand.Intn(128)))
				}
			}

		}()

		require.Equal(t, int64(1), GetMonitorAliveN())

		time.Sleep(time.Millisecond * 1500)
		t.Logf("%s", m.Info())
		cancel()
		wg.Wait()
		_ = m.Close()
		m = nil

	}

	for {
		runtime.GC()
		time.Sleep(time.Second)
		t.Logf("%d", GetMonitorAliveN())
	}

	require.Equal(t, int64(0), GetMonitorAliveN())
}

func runM(m *MonitorSpeedSizeRec, duration time.Duration) {
	t0 := time.Now()
	for {
		if time.Since(t0) > duration {
			break
		}
		m.Delta(int64(rand.Intn(512) + 10))
		time.Sleep(time.Millisecond)
	}

}

func TestMonitorBatch(t *testing.T) {
	//SetIntervalEngineer(NewTickerIntervalEnginner(time.Millisecond * 1500))

	var n atomic.Int32
	n0 := 80000
	var wg sync.WaitGroup
	for i := 0; i < n0; i++ {
		wg.Add(1)
		go func() {
			m := NewMonitorSpeedSize()
			defer func() {
				_ = m.Close()
				defer wg.Done()
				n.Add(1)
			}()
			t0 := time.Now()
			runM(m, time.Millisecond*time.Duration(rand.Intn(5000)+1500))
			require.NotZero(t, m.LastSpeedN, "dura:%d(ms) %s", time.Since(t0).Milliseconds(), m.Info())

		}()
	}

	time.Sleep(time.Millisecond * 100)

	wg.Wait()

	runtime.GC()
	time.Sleep(time.Second)
	runtime.GC()
	require.Equal(t, n.Load(), int32(n0))
	require.Equal(t, int64(0), GetMonitorAliveN())

}
