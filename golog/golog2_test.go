package golog_test

import (
	"gitee.com/ymofen/gobase"
	"gitee.com/ymofen/gobase/golog"
	"github.com/stretchr/testify/require"
	"os"
	"runtime"
	"testing"
	"time"
)

func BenchmarkParseGoLogToken(b *testing.B) {
	sfmt := golog.GoLogNormalFormat
	rec := &golog.GoLogRecord{
		Msg:         "abc",
		SourcePC:    uintptr(0),
		Level:       golog.LevelInfo,
		CreatedTime: time.Now(),
	}
	str := golog.ParseGoLogFormat(sfmt, func(token []byte) (data []byte, n int) {
		str, n := golog.GetGoLogFormatToken(rec, string(token))
		return []byte(str), n
	})
	b.Log(string(str))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		golog.ParseGoLogFormat(sfmt, func(token []byte) (data []byte, n int) {
			str, n := golog.GetGoLogFormatToken(rec, string(token))
			return []byte(str), n
		})
	}
}

func BenchmarkParse(b *testing.B) {
	sfmt := golog.GoLogNormalFormat

	rec := &golog.GoLogRecord{
		Msg:      "abc",
		SourcePC: uintptr(0),
		Level:    golog.LevelInfo,
	}
	str := golog.ParseGoLogFormat(sfmt, func(token []byte) (data []byte, n int) {
		str, n := golog.GetGoLogFormatToken(rec, string(token))
		return []byte(str), n
	})
	b.Log(string(str))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		golog.FormatLogRecord(sfmt, rec)
	}
}

func TestLog2(t *testing.T) {

	var logWrapper = func(msg string) {
		log := golog.Default().WithOptionEx(golog.WithOptionPrefix("INNER"), golog.WithOptionLevel(golog.LevelDebug))
		log.Info(msg)
	}

	w := gobase.NewMultiLogW()
	w.AddWriter(golog.NewWriterWrapper(func(bytes []byte) (int, error) {
		t.Log(string(bytes))
		return 0, nil
	}))
	// 创建一个新的
	log := golog.DefaultLogger().WithHandler(golog.WithFormatAndWriterHandler(w, ""))
	golog.SetDefaultLogger(log)

	golog.Default().LogMsgfWithOption(golog.LevelInfo, &golog.GoLogOption{Prefix: "PREFIX-YYYY", Level: golog.LevelDebug, AddSource: true}, "this is msg with option")

	log2 := golog.Default().WithOption(golog.GoLogOption{Prefix: "YMF", AddSource: true, Level: golog.LevelDebug})
	log2.Debugf("this message will show, new log")
	golog.Debugf("this message will not show")

	log.Infof("hello %s", "world")
	golog.Warnf("info hello %s", "world")

	log1 := golog.DefaultLogger().WithOption(golog.GoLogOption{Prefix: "YMF", AddSource: true})
	log1.Infof("hello %s", "111")

	golog.DefaultLogger().LogMsgWithSkip(golog.LevelInfo, 0, "helloworld with skip")

	logWrapper("this log wrapper")

}

func TestLog2Live(t *testing.T) {

	go func() {
		for {
			time.Sleep(time.Second * 2)
			runtime.GC()
			t.Logf("alive:%d", golog.GetGoLoggerAliveN())
		}
	}()

	time.Sleep(time.Second * 5)

	w := gobase.NewMultiLogW()
	w.AddWriter(os.Stdout)
	// 创建一个新的
	log := golog.DefaultLogger().WithHandler(golog.WithFormatAndWriterHandler(w, golog.GoLogNormalFormat))
	log.Infof("hello %s", "world")
	golog.Warnf("info hello %s", "world")

	time.Sleep(time.Second * 5)

	var innerLogFunc = func() {
		var innerLog *golog.GoLog
		var logMsg = func(msg string) {
			innerLog.LogMsg(golog.LevelInfo, "inner:"+msg)
		}
		// +1
		innerLog = log.WithOptionEx(golog.WithOptionPrefix("INNER"), golog.WithOptionCallSkip(1))
		require.Equal(t, int32(3), golog.GetGoLoggerAliveN())
		logMsg(gobase.NowString())

		//// -1
		//innerLog = nil
	}

	innerLogFunc()
	time.Sleep(time.Second * 5)
	require.Equal(t, int32(2), golog.GetGoLoggerAliveN())

	log.LogMsgf(golog.LevelInfo, "innerLogFunc Done")

	// -1
	log = nil
	time.Sleep(time.Second * 5)
	require.Equal(t, int32(1), golog.GetGoLoggerAliveN())

}
