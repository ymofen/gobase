package golog

type nullLogger struct {
}

func (l *nullLogger) Debug(s string) {

}

func (l *nullLogger) Info(s string) {

}

func (l *nullLogger) Warn(s string) {

}

func (l *nullLogger) Error(s string) {

}

func (l *nullLogger) Println(v ...interface{}) {}

func (l *nullLogger) Printf(format string, v ...interface{}) {}
func (l *nullLogger) Print(format string)                    {}

func (l *nullLogger) Fatal(v ...any) {

}

func (l *nullLogger) Fatalf(format string, v ...any) {

}

func (l *nullLogger) Warnf(s string, args ...interface{}) {

}

func (l *nullLogger) Infof(s string, args ...interface{}) {

}

func (l *nullLogger) Errorf(s string, args ...interface{}) {

}

func (l *nullLogger) Debugf(s string, args ...interface{}) {

}

var (
	NullLogger = &nullLogger{}
)
