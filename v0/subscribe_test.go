package v0

import (
	"bytes"
	"fmt"
	"gitee.com/ymofen/gobase"
	"math/rand"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestSubscribeV2Chk(t *testing.T) {
	sub := NewSubscribe()
	var exec_n int32
	var fn001 = func(id, topic string, args ...interface{}) (ok bool) {
		atomic.AddInt32(&exec_n, 1)
		return true
	}

	sub.Sub("001", "topic/+/key", fn001)

	sub.Sub("002", "topic/news", func(id, topic string, args ...interface{}) (ok bool) {
		atomic.AddInt32(&exec_n, 1)
		return true
	})

	n := int32(100)
	for i := int32(0); i < n; i++ {
		if sub.Pub(fmt.Sprintf("topic/%d/key", i), 0) != 1 {
			t.Fatal("pub err")
		}
	}

	if exec_n != n {
		t.Fatalf("err %d", exec_n)
	}

	if sub.Pub("topic/news", 0) != 1 {
		t.Fatal("pub err")
	}

	sub.Sub("001", "topic/a/key", fn001)
	if sub.Pub("topic/a/key", 0, 1, 2, 3) != 1 {
		t.Fatalf("pub err")
	}

	sub.Sub("002", "topic/a/key", fn001)
	if sub.Pub("topic/a/key", 0, 1, 2, 3) != 2 {
		t.Fatalf("pub err")
	}

	if sub.Pub("topic/a/key", 1, 1, 2, 3) != 1 {
		t.Fatalf("pub err")
	}

	if sub.Pub("topic/b/key", 0, 1, 2, 3) != 1 {
		t.Fatalf("pub err")
	}
}

func TestKV(t *testing.T) {

	{
		channel_max := 100000
		sub := NewSubscribe()
		t0 := time.Now()
		for i := 0; i < channel_max; i++ {
			id := fmt.Sprintf("topic/%X/%X", i, channel_max+i)
			channel := fmt.Sprintf("topic/+/%X", channel_max+i)
			sub.Sub(id, channel, func(id, topic string, args ...interface{}) (ok bool) {
				return true
			})
		}
		t1 := time.Now()
		t.Logf("%d, consume:%d(ms), speed:%d/ms", channel_max, t1.Sub(t0).Milliseconds(), int64(channel_max)/t1.Sub(t0).Milliseconds())

		pub_max := 100
		for i := 0; i < pub_max; i++ {
			i0 := i
			id := fmt.Sprintf("topic/%X/%X", i0%channel_max, channel_max+i0)
			topic := innerParseRoute(id)
			sub.matchTopic(topic, func(itm *SubTopicItem) bool {
				return true
			})
		}
		t2 := time.Now()
		ms := t2.Sub(t1).Milliseconds()
		t.Logf("%d, consume:%d(ms), speed:%d/s", pub_max, ms, int64(pub_max*1000)/ms)
	}

	{
		channel_max := 100000

		lst := make([]*SubTopicItem, 0, channel_max)

		var checkfind = func(topic string) *SubTopicItem {
			for i := 0; i < len(lst); i++ {
				if lst[i].subtopicid == topic {
					return lst[i]
				}
			}
			return nil
		}

		var rangeMatchFunc = func(topic interface{}, cb func(itm *SubTopicItem)) *SubTopicItem {
			for i := 0; i < len(lst); i++ {
				if innerRouteIncludesTopic(lst[i].subtopic, topic) {
					cb(lst[i])
				}
			}
			return nil
		}

		t0 := time.Now()
		for i := 0; i < channel_max; i++ {
			id := fmt.Sprintf("topic/%X/%X", i, channel_max+i)
			itm := checkfind(id)
			if itm == nil {
				itm = &SubTopicItem{subtopicid: id, subtopic: strings.Split(id, "/")}
				lst = append(lst, itm)
			}
		}
		t1 := time.Now()
		t.Logf("%d, consume:%d(ms), speed:%d/ms", channel_max, t1.Sub(t0).Milliseconds(), int64(channel_max)/t1.Sub(t0).Milliseconds())

		pub_max := 100
		for i := 0; i < pub_max; i++ {
			i0 := i
			id := fmt.Sprintf("topic/%X/%X", i0%channel_max, channel_max+i0)
			topic := innerParseRoute(id)
			rangeMatchFunc(topic, func(itm *SubTopicItem) {

			})
		}
		t2 := time.Now()
		ms := t2.Sub(t1).Milliseconds()
		t.Logf("%d, consume:%d(ms), speed:%d/s", pub_max, ms, int64(pub_max*1000)/ms)
	}
}

// cpu 92%
//
//	subscribe_test.go:203: workn:10000, 1911710, 5283(ms), 361860/s
//	subscribe_test.go:203: workn:10000, 3048468, 10287(ms), 296341/s
//	subscribe_test.go:203: workn:10000, 4041638, 15314(ms), 263917/s
//	subscribe_test.go:203: workn:10000, 4798290, 20324(ms), 236089/s
//	subscribe_test.go:203: workn:10000, 5399112, 25380(ms), 212730/s
//	subscribe_test.go:209: topic-n:0, sess:0, 5976438
func TestSubscribeV2MG_0(t *testing.T) {
	sub := NewSubscribe()
	var exec_n int32
	channel_max := 10000
	channels := make([]string, 0, 1024)
	databuf := make([][]byte, channel_max)
	for i := 0; i < channel_max; i++ {
		id := fmt.Sprintf("topic/%X/%X", i, channel_max+i)
		channels = append(channels, id)
		databuf[i] = gobase.RandBuf(rand.Intn(2048))
	}

	var runonce = func(id string, databuf []byte, dura time.Duration, channel string, beginfn, donefn func()) {
		t0 := time.Now()
		beginfn()
		sub.Sub(id, channel, func(id, topic string, args ...interface{}) (ok bool) {
			if bytes.Compare(args[0].([]byte), databuf) != 0 {
				t.Fatalf("recv err")
			}
			atomic.AddInt32(&exec_n, 1)
			return true
		})
		for time.Since(t0) < dura {
			sub.Pub(channel, 0, databuf, id, dura, channel)
			time.Sleep(0)
		}
		sub.Unsub(id, channel)
		donefn()
	}

	dura := time.Second * 30
	t0 := time.Now()

	var work_n int32 = 0
	var wt sync.WaitGroup
	for j := 0; j < 10000; j++ {
		wt.Add(1)
		go runonce(fmt.Sprintf("%X", j), databuf[j%len(databuf)], dura, channels[j%len(channels)], func() {
			atomic.AddInt32(&work_n, 1)
		}, func() {
			atomic.AddInt32(&work_n, -1)
			wt.Done()
		})
	}

	doneflag := 0

	go func() {
		for doneflag == 0 {
			time.Sleep(time.Second * 5)
			ms := time.Since(t0).Milliseconds()
			n := atomic.LoadInt32(&exec_n)
			t.Logf("workn:%d, %d, %d(ms), %d/s\r\n", work_n, n, ms, int64(n)*1000/int64(ms))
		}
	}()

	wt.Wait()
	doneflag = 1
	t.Logf("%s, %d", sub.Status(), exec_n)

}

// cpu: 95%
// subscribe_test.go:261: 36724, 7328/s
// subscribe_test.go:261: 74073, 7353/s
// subscribe_test.go:261: 112727, 7467/s
// subscribe_test.go:261: 150422, 7478/s
// subscribe_test.go:261: 184392, 7337/s
// subscribe_test.go:271: topic-n:10000, sess:10000, 215459, 215459
func TestSubscribeV2MG(t *testing.T) {
	sub := NewSubscribe()
	var exec_n int32
	var pub_n int32
	channel_max := 10000
	channellst := make([]string, channel_max)

	// 订阅N个通道数据
	for i := 0; i < channel_max; i++ {
		id := fmt.Sprintf("id-%X", i%channel_max)
		channel := fmt.Sprintf("topic/%X", i%channel_max)
		channellst[i] = channel
		sub.Sub(id, channel, func(id, topic string, args ...interface{}) (ok bool) {
			atomic.AddInt32(&exec_n, 1)
			return true
		})

		time.AfterFunc(time.Second*20, func() {
			sub.Unsub(id, channel)
		})

		//sub.Pub(channel, 0)
	}

	t0 := time.Now()
	dura := time.Second * 30
	var wt sync.WaitGroup
	for j := 0; j < 10; j++ {

		wt.Add(1)
		go func() {
			idx := 0
			for time.Since(t0) < dura {
				channelsn := idx % channel_max
				if channelsn == 0 {
					channelsn = 0
				}

				channel := channellst[channelsn]
				n := sub.Pub(channel, 0)
				atomic.AddInt32(&pub_n, int32(n))
				time.Sleep(0)
				idx++
			}
			wt.Done()
		}()
	}

	doneflag := 0

	go func() {
		for doneflag == 0 {
			time.Sleep(time.Second * 5)
			n0 := atomic.LoadInt32(&exec_n)
			t.Logf("%d, %d/s\r\n", n0, int64(n0*1000)/time.Since(t0).Milliseconds())
		}
	}()

	wt.Wait()
	doneflag = 1
	if exec_n != pub_n {
		t.Fatalf("err")
	}

	t.Logf("%s, %d, %d", sub.Status(), exec_n, pub_n)

}
