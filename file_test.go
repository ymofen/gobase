package gobase_test

import (
	"fmt"
	"gitee.com/ymofen/gobase"
	_ "gitee.com/ymofen/gobase/golog"
	"github.com/stretchr/testify/require"
	"log"
	"runtime"
	"testing"
	"time"
)

func TestPanic(t *testing.T) {
	gobase.EnablePanicRecordFile("log")
	log.SetFlags(log.Lshortfile)
	gobase.LogPrintf("hello:%s", gobase.NowString())
	go func() {
		if gobase.GoFunCatchException {
			defer gobase.DeferCatchPanic()
		}
		buf := gobase.RandBuf(24)
		t.Logf("%d", buf[25])

		t.Logf("%s", "hello world")
	}()
	time.Sleep(time.Second * 10)

}

func TestPanicFatal(t *testing.T) {
	gobase.EnablePanicRecordFile("log")
	fname := gobase.ParseFileName("log/$(exename)-$(pid)-$(yyyymmdd).log")
	gobase.SetFatalWriteFile(fname)
	gobase.LogPrintf("hello:%s", gobase.NowString())
	go func() {
		buf := gobase.RandBuf(24)
		t.Logf("%d", buf[25])

		t.Logf("%s", "hello world")
	}()
	time.Sleep(time.Second * 10)
}

func TestLogMsg(t *testing.T) {
	log.SetFlags(log.Lshortfile)
	gobase.SetLogOutput(func(calldepth int, lvl int8, sender string, s string) {
		pc, _, lineno, _ := runtime.Caller(calldepth)
		src := fmt.Sprintf("%s:%d", runtime.FuncForPC(pc).Name(), lineno)
		t.Logf("%s, %s", src, s)
	})
	gobase.LogPrintf("hello1:%s", gobase.NowString())
	gobase.SetLogOutput(nil)
	gobase.LogPrintf("hello2:%s", gobase.NowString())

	time.Sleep(time.Second)
}

func TestGetRelativeFileName(t *testing.T) {
	{
		baseDir := `E:\temp\gnss-trans`
		fileName := `E:\temp\gnss-trans\2025073\bbo23575.sp3`
		rfile := gobase.GetRelativeFileName(baseDir, fileName, '\\')
		require.Equal(t, "2025073\\bbo23575.sp3", rfile)
	}

	{
		baseDir := `E:\temp\gnss-trans\2025072\`
		fileName := `E:\temp\gnss-trans\2025073\bbo23575.sp3`
		rfile := gobase.GetRelativeFileName(baseDir, fileName, '/')
		require.Equal(t, "../2025073/bbo23575.sp3", rfile)
	}

}

func TestFileNameMatch(t *testing.T) {
	// 新添加的测试用例
	testCases := []struct {
		pattern  string
		filename string
		expected bool
	}{
		{"*", "中国/牛逼Glass.Abc", true},
		{"中国/*", "中国/牛逼Glass.Abc", true},
		{"中国/*.A", "中国/牛逼Glass.Abc", false},
		{"中国/*.A?c", "中国/牛逼Glass.Abc", true},
		{"?国/*.A?c", "中国/牛逼Glass.Abc", true},
		{"中国/[中A].Abc", "中国/A.Abc", true},
		{"中国/[!A].Abc", "中国/A.Abc", false},
		{"中国/[!AB].Abc", "中国/C.Abc", true},
		{"中国/｛A,中A｝.Abc", "中国/中A.Abc", true},
		{"中国/｛A,中A｝.Abc", "中国/C.Abc", false},
		{"中国/｛A,中B｝.Abc", "中国/中A.Abc", false},
		{"*中国/｛A,中A｝.Abc", "地球/中国/C.Abc", false},
		{"*中国/｛A,中A｝.Abc", "地球/中国/A.Abc", true},
		{"*/中国/｛A,中A｝.Abc", "地球/中国/A.Abc", true},
		{"??/中国/｛A,中A｝.Abc", "地球/中国/A.Abc", true},
		{"*/中国/｛A,中A｝.Abc", "宇宙/地球/中国/A.Abc", true},
	}

	for _, tc := range testCases {
		result := gobase.FileNameMatch(tc.pattern, tc.filename)
		if result == tc.expected {
			t.Logf("测试通过: 模式 %s 匹配 %s 结果符合预期\n", tc.pattern, tc.filename)
		} else {
			t.Errorf("测试失败: 模式 %s 匹配 %s 结果不符合预期，期望 %v，实际 %v\n", tc.pattern, tc.filename, tc.expected, result)
		}
	}

}
