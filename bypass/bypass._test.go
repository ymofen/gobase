package bypass

import (
	"math/rand"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestByPassGroup(t *testing.T) {
	byPass := NewByPassGroup()
	byPass.BeginUpdate()
	byPass.ConfigStrategy(0)
	byPass.UpdateByPassNode("r11_0", 0, 1000, "connstr=r11_0")
	byPass.UpdateByPassNode("r11_1", 1, 100, "connstr=r11_1")
	byPass.EndUpdate(true)

	var succ_n int32 = 0
	var fail_n int32 = 0

	start0 := time.Now()

	var logstatus = func(prefix string) {
		ms := time.Since(start0).Milliseconds()
		t.Logf("%ssucc:%d, fail:%d, speed:%d/ms, status:\n%s", prefix, succ_n, fail_n, int64(succ_n)/ms, byPass.Status())
	}

	var runOnce = func(d time.Duration, donefn func()) {
		defer donefn()
		t0 := time.Now()
		for time.Since(t0) < d {
			itm := byPass.GetByPassNode()
			if itm == nil {
				atomic.AddInt32(&fail_n, 1)
			} else {
				atomic.AddInt32(&succ_n, 1)
				time.Sleep(time.Millisecond)
				byPass.ReleaseByPassNode(itm, false, 0, "")
			}
			time.Sleep(time.Millisecond)
		}
	}

	go func() {
		for {
			time.Sleep(time.Second * 10)
			logstatus("")
		}
	}()

	var wg sync.WaitGroup
	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go runOnce(time.Second*time.Duration(rand.Intn(60)), func() {
			wg.Done()
		})
	}

	wg.Wait()

	logstatus("===========\n")

}

func TestByPassMananger(t *testing.T) {
	byPass := NewByPassManager()

	var reloadConf = func() {
		flag := rand.Intn(3)
		{
			byPassGroup := byPass.CheckGetByPassGroup("r11", true)
			byPassGroup.BeginUpdate()
			byPassGroup.ConfigStrategy(2)
			byPassGroup.UpdateByPassNode("r11_0", 0, 0, "connstr=r11_0")
			byPassGroup.UpdateByPassNode("r11_1", 1, 0, "connstr=r11_1")
			byPassGroup.EndUpdate(true)

			if flag == 1 {
				byPassGroup.DeleteNode("r11_0")
			} else if flag == 2 {
				byPassGroup.BlockNode("r11_1", time.Second*10)
			} else if flag == 3 {
				byPassGroup.BlockNode("r11_1", 0)
			}
		}

		{
			byPassGroup := byPass.CheckGetByPassGroup("r12", true)
			byPassGroup.BeginUpdate()
			byPassGroup.ConfigStrategy(0)
			byPassGroup.UpdateByPassNode("r12_0", 0, rand.Intn(1000), "connstr=r12_0")
			byPassGroup.UpdateByPassNode("r12_1", 1, rand.Intn(20000), "connstr=r12_1")
			byPassGroup.EndUpdate(true)
		}

		{
			byPassGroup := byPass.CheckGetByPassGroup("r13", true)
			byPassGroup.BeginUpdate()
			byPassGroup.ConfigStrategy(1)
			byPassGroup.UpdateByPassNode("r13_0", 0, rand.Intn(1000), "connstr=r13_0")
			byPassGroup.UpdateByPassNode("r13_1", 1, rand.Intn(20000), "connstr=r13_1")
			byPassGroup.EndUpdate(true)
		}
		byPass.TrySetDefault("rxx")
	}

	reloadConf()

	go func() {
		for {
			time.Sleep(time.Second * time.Duration(rand.Intn(10)))
			reloadConf()
		}
	}()

	var succ_n int32 = 0
	var fail_n int32 = 0

	start0 := time.Now()

	var logstatus = func(prefix string) {
		ms := time.Since(start0).Milliseconds()
		t.Logf("%ssucc:%d, fail:%d, speed:%d/ms, status:\n%s", prefix, succ_n, fail_n, int64(succ_n)/ms, byPass.GetStatusText(0))
	}

	var runOnce = func(groupid string, d time.Duration, donefn func()) {
		defer donefn()
		time.Sleep(time.Millisecond * time.Duration(rand.Intn(5000)))
		t0 := time.Now()
		for time.Since(t0) < d {
			itm := byPass.GetByPassNode(groupid)
			if itm == nil {
				atomic.AddInt32(&fail_n, 1)
			} else {
				atomic.AddInt32(&succ_n, 1)
				// 				time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
				time.Sleep(time.Millisecond)
				byPass.ReleaseByPassNode(itm, false, 0, "")
			}
			time.Sleep(time.Millisecond)
		}
	}

	go func() {
		for {
			time.Sleep(time.Second * 5)
			logstatus("")
		}
	}()

	var wg sync.WaitGroup
	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go runOnce("r11", time.Second*time.Duration(rand.Intn(60)), func() {
			wg.Done()
		})
	}

	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go runOnce("r12", time.Second*time.Duration(rand.Intn(60)), func() {
			wg.Done()
		})
	}

	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go runOnce("r13", time.Second*time.Duration(rand.Intn(60)), func() {
			wg.Done()
		})
	}

	wg.Wait()

	logstatus("===========\n")

}

func TestByPassUsage(t *testing.T) {
	byPass := NewByPassManager()
	{
		byPassGroup := byPass.CheckGetByPassGroup("r11", true)
		byPassGroup.BeginUpdate()
		byPassGroup.ConfigStrategy(0)
		byPassGroup.UpdateByPassNode("r11_0", 0, 1000, "connstr=r11_0")
		byPassGroup.UpdateByPassNode("r11_1", 1, 0, "connstr=r11_1")
		byPassGroup.EndUpdate(true)
	}

	{
		byPassGroup := byPass.CheckGetByPassGroup("r12", true)
		byPassGroup.BeginUpdate()
		byPassGroup.ConfigStrategy(1)
		byPassGroup.UpdateByPassNode("r12_0", 0, 1000, "connstr=r12_0")
		byPassGroup.UpdateByPassNode("r12_1", 1, 0, "connstr=r12_1")
		byPassGroup.EndUpdate(true)
	}

	var succ_n int32 = 0
	var fail_n int32 = 0

	start0 := time.Now()

	var logstatus = func(prefix string) {
		ms := time.Since(start0).Milliseconds()
		t.Logf("%ssucc:%d, fail:%d, speed:%d/ms, status:\n%s", prefix, succ_n, fail_n, int64(succ_n)/ms, byPass.GetStatusText(1))
	}

	var runOnce = func(groupid string, d time.Duration, donefn func()) {
		defer donefn()
		time.Sleep(time.Millisecond * time.Duration(rand.Intn(1000)))
		t0 := time.Now()
		for time.Since(t0) < d {
			itm := byPass.GetByPassNode(groupid)
			if itm == nil {
				atomic.AddInt32(&fail_n, 1)
			} else {
				atomic.AddInt32(&succ_n, 1)
				time.Sleep(time.Millisecond)
				byPass.ReleaseByPassNode(itm, false, 0, "")
			}
			time.Sleep(time.Millisecond)
		}
	}

	var wg sync.WaitGroup

	wg.Add(1)
	go runOnce("r12", time.Second*time.Duration(rand.Intn(60)), func() {
		wg.Done()
	})

	time.AfterFunc(time.Second, func() {
		byPassGroup := byPass.CheckGetByPassGroup("r12", true)
		byPassGroup.UpdateByPassNode("r12_0", 0, 1000, "connstr=r12_0")
	})

	wg.Wait()

	logstatus("===========\n")

}
