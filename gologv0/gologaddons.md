## SingleCacheFileWriter
    缓存文件写入, 写入时采用排队方式进行写入

### 方法
#### SetMsgFmt 消息写入格式
    默认格式:%P:%L %u %M (%s)\r\n
    %P 消息类型
    %L 消息等级 
    %u UTC时间 23-11-13 21:50:56 
    %M 消息 
    %s 调用信息

#### OnLogMsg(rec *GoLogRecord) bool
    提供给 GoLog 进行加入到日志写入器中

#### WriteMsgf(msglvl int8, s string, args ...interface{}) bool
    直接写入文件, 不受SetMsgFmt格式影响
    true, 表示写入成功

#### (this *SingleCacheFileWriter) SetMaxSize(v int)
    设置文件最大尺寸, 超过该大小时会重命名文件到:filename.20231114.n 并调用go OnFileEof