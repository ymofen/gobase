package binpack

import (
	"fmt"
	"gitee.com/ymofen/gobase"
	"io"
)

const (
	// msgid 0xFFF 0-4095
	// data 0xFFFF max:65535
	// 包协议, minL: 8=1+2+2+3, bodystart:4
	// pre[1]+0xA(4bit)msgid(12bit)+dataL(LE:2)+data+crc24[3]

	BinPack_MinL         = 8
	BinPack_DataStartIdx = 5

	BinPack_PRE0      byte = 0xF1
	BinPack_PRE1_4Bit      = 0xA
)

func WriteBinPack(w io.Writer, msgid uint16, data []byte) (err error) {
	w.Write([]byte{BinPack_PRE0})

	l1 := msgid
	l2 := len(data)
	if l1 > 0xFFF {
		return fmt.Errorf("msgid must less than %d", 0xFFF)
	}

	if l2 > 0xFFFF {
		return fmt.Errorf("data size must less than %d", 0xFFFF)
	}

	crc := uint32(0)
	l1 = 0xA000 | l1
	buf := []byte{byte(l1 >> 8), byte(l1), byte(l2 >> 8), byte(l2)}

	w.Write(buf)
	crc = gobase.CRC24QContinue(crc, buf...)

	w.Write(data)
	crc = gobase.CRC24QContinue(crc, data...)

	w.Write([]byte{byte(crc >> 16), byte(crc >> 8), byte(crc)})
	return nil
}

type BinPackDecode struct {
	decodestep int8

	msgid  uint16
	datal  int
	cachel int
	cache  gobase.BytesBuilder
}

func (this *BinPackDecode) ExtractMsg() (msgid uint16, data []byte) {
	return this.msgid, this.cache.Buff(BinPack_DataStartIdx, this.datal)
}

// 0: need more
// 1: ok
// -1: crc
// -2: errdata
func (this *BinPackDecode) InputByte(v byte) int {
	if this.decodestep < 0 {
		this.cache.Reset()
		this.cachel = 0
		this.datal = 0
		this.decodestep = 1
	}

	this.cache.WriteByte(v)
	this.cachel++
	if this.cachel >= BinPack_MinL {
		if this.cachel < this.datal+BinPack_MinL {
			return 0
		} else {
			crc0 := this.cache.Uint32_BE(this.cachel - 4)
			crc0 = crc0 & 0x00FFFFFF
			crc1 := gobase.CRC24QBuf(this.cache.Buff(1, this.datal+4))
			this.decodestep = -1
			if crc1 != crc0 {
				return -1
			}
			return 1
		}
	}
	if this.cachel == 1 {
		if v != BinPack_PRE0 {
			this.decodestep = -1
			return -2
		}
	} else if this.cachel == 2 {
		if v&0xF0 != 0xA0 {
			this.decodestep = -1
			return -2
		}
	} else if this.cachel == 3 {
		//this.msgid = uint16(this.cache.Byte(1)&0x0F)<<8 + uint16(this.cache.Byte(2))
		this.msgid = this.cache.Uint16_BE(1) & 0x0FFF
	} else if this.cachel == 5 {
		this.datal = int(this.cache.Uint16_BE(3))
	}

	return 0
}
