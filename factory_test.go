package gobase

import (
	"fmt"
	"gitee.com/ymofen/gobase/gocache"
	"github.com/stretchr/testify/require"
	"os"
	"testing"
)

func TestReplacePlaceholder(t *testing.T) {

	{
		gocache.Set("vrstype", "yd")
		str := `$rt{$cache.vrstype)$`
		str = ReplacePlaceholder(str, "$rt{", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExpWithFunc(key, TryGetFactoryTokenValue)
		})

		require.Contains(t, str, "yd")
	}

	{
		RegisterFactoryVar("v", "STA-HB02")
		str := `$rt{$v mod 3)$`
		str = ReplacePlaceholder(str, "$rt{", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExpressionAndVar(key)
		})

		require.Contains(t, str, "invalid [mod] args")
	}

	{
		RegisterFactoryVar("v", "STA-HB02")
		str := `$rt{$v mod )$`
		str = ReplacePlaceholder(str, "$rt{", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExpressionAndVar(key)
		})

		require.Contains(t, str, "invalid [mod] args")
	}

	{
		RegisterFactoryVar("v", "STA-HB02")
		str := `$rt{$v)$`

		str = ReplacePlaceholder(str, "$rt{", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExpressionAndVar(key)
		})

		require.Equal(t, "STA-HB02", str)
	}

	{
		RegisterFactoryVar("v", "STA-HB02")
		str := `${$v hash)$`

		str = ReplacePlaceholder(str, "${", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExp(key)
		})

		require.Equal(t, fmt.Sprintf("%d", HashStr("STA-HB02")), str)
	}

	{
		RegisterFactoryVar("v", "STA-HB02")
		str := `$($v hash % 10)$`

		str = ReplacePlaceholder(str, "$(", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExp(key)
		})

		t.Logf("%s", str)
	}

	{
		RegisterFactoryVar("v", "STA-HB02")
		str := `$($v hash mod 10)$`

		str = ReplacePlaceholder(str, "$(", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExp(key)
		})

		t.Logf("%s", str)
	}

	{
		os.Setenv("hostname", "algs-stapull-7bfc46d877-v9hls")

		str := `$($env.hostname spidx '-' 3)$`

		str = ReplacePlaceholder(str, "$(", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExp(key)
		})

		t.Logf("%s", str)

		require.Equal(t, "v9hls", str)
	}

	{
		os.Setenv("hostname", "algs-7bfc46d877-v9hls")

		str := `$($env.hostname splastidx '-' 0)$`

		str = ReplacePlaceholder(str, "$(", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExp(key)
		})

		t.Logf("%s", str)

		require.Equal(t, "v9hls", str)
	}

}

// BenchmarkReplacePlaceholder-16    	  556600	      2151 ns/op
func BenchmarkReplacePlaceholder(t *testing.B) {
	RegisterFactoryVar("v", "STA-HB02")

	for i := 0; i < t.N; i++ {
		str := `COPY-$($v hash % 10)$`

		str = ReplacePlaceholder(str, "$(", ")$", func(key string) (v string, ok bool) {
			return ParseSimpleExp(key)
		})
		require.Equal(t, "COPY-3", str)
	}

}
