package gobase

import (
	"testing"
)

func TestCRC24Q(t *testing.T) {
	buf := []byte("中国ABC\r\n")

	v1 := CRC24QBuf(buf)

	buf1 := buf[:2]
	v2 := CRC24QContinue(0, buf1...)
	v2 = CRC24QContinue(v2, buf[2:]...)

	if v1 != v2 {
		t.Fatal("err")
	}
}
