package main

import (
	"gitee.com/ymofen/gobase"
	"gitee.com/ymofen/gobase/golog"
	"time"
)

func main() {
	gobase.EnablePanicRecordFile("log")
	fname := gobase.ParseFileName("log/$(exename)-fatal-$(pid)-$(yyyymmdd).log")
	_ = gobase.SetFatalWriteFile(fname)
	gobase.LogPrintf("hello:%s", gobase.NowString())
	go func() {
		golog.Infof("hello in funcs")
		buf := gobase.RandBuf(24)
		golog.Infof("%d", buf[25])

		//golog.Infof("%s", "hello world")
	}()
	time.Sleep(time.Second * 10)
}
