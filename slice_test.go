package gobase_test

import (
	"gitee.com/ymofen/gobase"
	"github.com/stretchr/testify/require"
	"math/rand"
	"slices"
	"sort"
	"strings"
	"testing"
)

func TestIndexEle(t *testing.T) {
	{
		lst := []string{"*", "a", "b", "d"}
		labe := []string{"b", "a"}

		idx := gobase.IndexEle(lst, labe)
		require.Equal(t, 2, idx)
	}

	{
		lst := []string{"*", "a", "b", "d"}
		labe := []string{"ee", "aa"}

		idx := gobase.IndexEle(lst, labe)
		require.Equal(t, -1, idx)
	}
}

func TestIndexEleSorted(t *testing.T) {
	{
		lst := []string{"*", "a", "b", "d"}
		labe := []string{"a", "d"}

		idx := gobase.IndexEleSorted(lst, labe)
		require.Equal(t, 1, idx)
	}

	{
		lst := []string{"*", "a", "b", "d"}
		labe := []string{"aa", "ee"}

		idx := gobase.IndexEleSorted(lst, labe)
		require.Equal(t, -1, idx)
	}

	{
		lstb := []byte(gobase.RandPass0String(8))
		slices.Sort(lstb)
		for i := 0; i < 100; i++ {
			lsta := []byte(gobase.RandPass0String(8192))
			slices.Sort(lsta)

			idx1 := gobase.IndexEle(lsta, lstb)
			idx2 := gobase.IndexEleSorted(lsta, lstb)
			require.Equal(t, idx1, idx2)

		}
	}

}

func BenchmarkIndexEle(b *testing.B) {

	// // BenchmarkIndexEle-16    	 1915742	       610.0 ns/op
	//lstb := []byte(gobase.RandPass0String(8))
	//lsta := []byte(gobase.RandPass0String(8192))

	// BenchmarkIndexEle-16    	 1000000	      1185 ns/op
	lsta := e_text
	lstb := []string{"opportunity", "chapter", "growth"}

	//slices.Sort(lstb)
	//slices.Sort(lsta)
	idx1 := gobase.IndexEle(lsta, lstb)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		idx2 := gobase.IndexEle(lsta, lstb)
		require.Equal(b, idx1, idx2)
	}
}

func BenchmarkIdxEleSorted(b *testing.B) {

	// BenchmarkIdxEleSorted-16    	  136202	      9561 ns/op
	//lstb := []byte(gobase.RandPass0String(8))
	//lsta := []byte(gobase.RandPass0String(8192))

	// BenchmarkIdxEleSorted-16    	  721158	      1697 ns/op
	lsta := e_text
	lstb := []string{"opportunity", "chapter", "growth"}

	slices.Sort(lstb)
	slices.Sort(lsta)
	idx1 := gobase.IndexEle(lsta, lstb)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		idx2 := gobase.IndexEleSorted(lsta, lstb)
		require.Equal(b, idx1, idx2)
	}
}

func TestDeleteEleWithFunc(t *testing.T) {
	{
		lst := []string{"a", "b", "c", "d"}
		lst = gobase.DeleteEleWithFunc(lst, func(e string) (delflag bool) {
			if e == "b" {
				return true
			}
			return false
		})
		require.Equal(t, lst, []string{"a", "c", "d"})
	}

	{
		lst := []string{"a", "b", "c", "d"}
		lst = gobase.DeleteEleWithFunc(lst, func(e string) (delflag bool) {
			if e == "b" || e == "c" {
				return true
			}
			return false
		})
		require.Equal(t, lst, []string{"a", "d"})
	}

	{
		lst := []string{"a", "b", "c"}
		lst = gobase.DeleteEleWithFunc(lst, func(e string) (delflag bool) {
			return e == "a" || e == "b"
		})
		require.Equal(t, lst, []string{"c"})
	}

	{
		lst := []string{"a", "b", "c"}
		lst = gobase.DeleteEleWithFunc(lst, func(e string) (delflag bool) {
			return e == "a" || e == "b" || e == "c"
		})
		require.Equal(t, lst, []string{})
	}

	{
		lst := []string{"a", "b", "c"}
		lst = gobase.DeleteEleWithFunc(lst, func(e string) (delflag bool) {
			return e == "c"
		})
		require.Equal(t, lst, []string{"a", "b"})
	}

}

func TestDeleteEleWithFunc2(t *testing.T) {
	{
		lst := []string{"a", "b", "c", "d"}
		lst = gobase.DeleteStrlstEle(lst, func(e string) (delflag bool) {
			if e == "b" {
				return true
			}
			return false
		})
		require.Equal(t, lst, []string{"a", "c", "d"})
	}

	{
		lst := []string{"a", "b", "c", "d"}
		lst = gobase.DeleteStrlstEle(lst, func(e string) (delflag bool) {
			if e == "b" || e == "c" {
				return true
			}
			return false
		})
		require.Equal(t, lst, []string{"a", "d"})
	}

	{
		lst := []string{"a", "b", "c"}
		lst = gobase.DeleteStrlstEle(lst, func(e string) (delflag bool) {
			return e == "a" || e == "b"
		})
		require.Equal(t, lst, []string{"c"})
	}

	{
		lst := []string{"a", "b", "c"}
		lst = gobase.DeleteStrlstEle(lst, func(e string) (delflag bool) {
			return e == "a" || e == "b" || e == "c"
		})
		require.Equal(t, lst, []string{})
	}

	{
		lst := []string{"a", "b", "c"}
		lst = gobase.DeleteStrlstEle(lst, func(e string) (delflag bool) {
			return e == "c"
		})
		require.Equal(t, lst, []string{"a", "b"})
	}

}

func TestFindSortedListDifferencesChk(t *testing.T) {
	{
		s000 := strings.Split("CKBS,CQ01,CQ02", ",")
		s001 := strings.Split("CKBS,CQ02,CQ03", ",")
		a, d := gobase.FindListDifferences(s000, s001)
		require.Equal(t, []string{"CQ03"}, a)
		require.Equal(t, []string{"CQ01"}, d)

		t.Logf("adds:%s, del:%s", strings.Join(a, ","), strings.Join(d, ","))
	}

	{
		s000 := strings.Split("CKBS,CQ01,CQ02", ",")
		s001 := strings.Split("CKBS,CQ02", ",")
		a, d := gobase.FindListDifferences(s000, s001)
		require.Equal(t, 0, len(a))
		require.Equal(t, []string{"CQ01"}, d)

		t.Logf("adds:%s, del:%s", strings.Join(a, ","), strings.Join(d, ","))
	}

	{
		s000 := strings.Split("CKBS,CQ01,CQ02", ",")
		s001 := strings.Split("CKBS,CQ01,CQ05,CQ02", ",")
		a, d := gobase.FindListDifferences(s000, s001)
		require.Equal(t, []string{"CQ05"}, a)
		require.Equal(t, 0, len(d))

		t.Logf("adds:%s, del:%s", strings.Join(a, ","), strings.Join(d, ","))
	}
}

func TestFindSortedListDifferences(t *testing.T) {
	s000 := strings.Split("CKBS,CQ01,CQ02,CQ03,CQ04,CQ05,CQ06,CQ07,CQ24,CQFJ,CQMZ,CQPL,CQPS,CQSZ,BDSB,CQWS,CQWZ,CQXS,CQYY,CQZX,DZWY,DZXH,ESHF,ESTJ,FJJG,FXDM,HB13,HB14,HB15,HB16,HB17,HB19,HB26,HB27,HB28,HB32,HB33,HB35,HB37,HB38,HB43,HB44,HB46,HB48,HB53,HB55,HB61,HB62,HB71,HB72,HB73,HB75,HB77,HB79,HBBF,HBBK,HBDL,HBFX,HBHF,HBLF,HBSN,HBYC,JZSZ,KZJC,LCZL,SC37,SC76,SCWY,SYMC,SYWD,SYZX,WLHP,WSPH,WXCS,WXCY,WZGC,XHNB,XYBK,YCWJ,YCYL,YCZG,YYBX,YYSP,YYTJ,YYWJ", ",")
	var s0, s1 []string

	for i := 0; i < len(s000)-5; {
		s := s000[rand.Intn(len(s000)-1)]
		if gobase.StrIndex(s, s0...) == -1 {
			s0 = append(s0, s)
			i++
		}
	}

	for i := 0; i < len(s000)-5; {
		s := s000[rand.Intn(len(s000)-1)]
		if gobase.StrIndex(s, s0...) == -1 {
			s1 = append(s1, s)
			i++
		}
	}

	a, d := gobase.FindListDifferences[string](s0, s1)
	for _, s := range a {
		require.Equal(t, -1, gobase.StrIndex(s, s0...))
	}

	// d, s0存在, s1不存在
	for _, s := range d {
		require.Equal(t, -1, gobase.StrIndex(s, s1...))
	}

	// d, s0存在, s1不存在
	for _, s := range d {
		require.NotEqual(t, -1, gobase.StrIndex(s, s0...))
	}

}

// BenchmarkFindListDifferences2-8   	  208826	      5970 ns/op
func BenchmarkFindListDifferences2(t *testing.B) {

	s000 := strings.Split("CKBS,CQ01,CQ02,CQ03,CQ04,CQ05,CQ06,CQ07,CQ24,CQFJ,CQMZ,CQPL,CQPS,CQSZ,BDSB,CQWS,CQWZ,CQXS,CQYY,CQZX,DZWY,DZXH,ESHF,ESTJ,FJJG,FXDM,HB13,HB14,HB15,HB16,HB17,HB19,HB26,HB27,HB28,HB32,HB33,HB35,HB37,HB38,HB43,HB44,HB46,HB48,HB53,HB55,HB61,HB62,HB71,HB72,HB73,HB75,HB77,HB79,HBBF,HBBK,HBDL,HBFX,HBHF,HBLF,HBSN,HBYC,JZSZ,KZJC,LCZL,SC37,SC76,SCWY,SYMC,SYWD,SYZX,WLHP,WSPH,WXCS,WXCY,WZGC,XHNB,XYBK,YCWJ,YCYL,YCZG,YYBX,YYSP,YYTJ,YYWJ", ",")
	var s0, s1 []string

	for i := 0; i < len(s000)-5; {
		s := s000[rand.Intn(len(s000)-1)]
		if gobase.StrIndex(s, s0...) == -1 {
			s0 = append(s0, s)
			i++
		}
	}

	for i := 0; i < len(s000)-5; {
		s := s000[rand.Intn(len(s000)-1)]
		if gobase.StrIndex(s, s0...) == -1 {
			s1 = append(s1, s)
			i++
		}
	}

	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		s00 := s0
		s01 := s1

		sort.Slice(s00, func(i, j int) bool {
			return s00[i] < s00[j]
		})

		sort.Slice(s01, func(i, j int) bool {
			return s01[i] < s01[j]
		})
		gobase.FindSortedListDifferences[string](s00, s01)
	}
}

// BenchmarkFindListDifferences-FindListDifferences2   	   49141	     24709 ns/op
// BenchmarkFindListDifferences-FindListDifferences   	  789764	      2465 ns/op
func BenchmarkFindListDifferences(t *testing.B) {
	s000 := strings.Split("CKBS,CQ01,CQ02,CQ03,CQ04,CQ05,CQ06,CQ07,CQ24,CQFJ,CQMZ,CQPL,CQPS,CQSZ,BDSB,CQWS,CQWZ,CQXS,CQYY,CQZX,DZWY,DZXH,ESHF,ESTJ,FJJG,FXDM,HB13,HB14,HB15,HB16,HB17,HB19,HB26,HB27,HB28,HB32,HB33,HB35,HB37,HB38,HB43,HB44,HB46,HB48,HB53,HB55,HB61,HB62,HB71,HB72,HB73,HB75,HB77,HB79,HBBF,HBBK,HBDL,HBFX,HBHF,HBLF,HBSN,HBYC,JZSZ,KZJC,LCZL,SC37,SC76,SCWY,SYMC,SYWD,SYZX,WLHP,WSPH,WXCS,WXCY,WZGC,XHNB,XYBK,YCWJ,YCYL,YCZG,YYBX,YYSP,YYTJ,YYWJ", ",")
	var s0, s1 []string

	for i := 0; i < len(s000)-5; {
		s := s000[rand.Intn(len(s000)-1)]
		if gobase.StrIndex(s, s0...) == -1 {
			s0 = append(s0, s)
			i++
		}
	}

	for i := 0; i < len(s000)-5; {
		s := s000[rand.Intn(len(s000)-1)]
		if gobase.StrIndex(s, s0...) == -1 {
			s1 = append(s1, s)
			i++
		}
	}

	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		gobase.FindListDifferences[string](s0, s1)
	}
}

var (
	e_text = gobase.DeleteEmpty(strings.Split(`Title: Year-End Reflection: A Journey Through 2023

As the calendar flips to its final page, marking the end of another chapter in our lives, it's a time filled with both reflection and anticipation. 2023 has been a year of unparalleled changes, challenges, and triumphs, each shaping our journey in unique and profound ways. Here's a回顾 of the year gone by, highlighting the lessons learned, achievements unlocked, and the hopes carried forward into the new dawn.

Personal Growth: Embracing Change

The year began with a resolution to embrace change, recognizing that growth often stems from stepping out of one's comfort zone. I took up new hobbies like meditation and painting, which not only served as outlets for creativity but also fostered a deeper sense of self-awareness. Through these experiences, I learned the value of patience and the art of letting go, teaching me to navigate life's uncertainties with a more serene mindset.

Professional Milestones: Climbing New Heights

Professionally, 2023 was marked by significant strides. I embarked on a new project at work that pushed the boundaries of my expertise, requiring late nights of research, team collaboration, and innovative thinking. Despite the initial hurdles, the project's success at the end of the year was a testament to our resilience and dedication. It reinforced the belief that hard work, coupled with a positive attitude, can turn even the most daunting tasks into achievable goals.

Challenges Faced: Building Resilience

Challenges were inevitable, serving as stepping stones in my personal and professional growth. A family health scare mid-year reminded me of the fragility of life and the importance of cherishing every moment. It taught me the power of resilience, as we rallied together, found strength in each other's support, and emerged stronger on the other side. This experience underscored the need for better health habits and a stronger commitment to loved ones.

Community Involvement: Spreading Positivity

Volunteering became a cornerstone of my year, offering me an opportunity to give back and make a positive impact. From participating in local clean-up drives to mentoring young professionals, these acts of service filled my heart with joy and a sense of belonging. They reminded me that even the smallest contributions can ripple outward, creating waves of positivity and change.

Looking Ahead: Embracing the Unknown

As we stand on the threshold of 2024, I carry forward a heart full of gratitude for the experiences and lessons of 2023. The year has taught me the importance of balance, resilience, and the continuous pursuit of personal growth. With a renewed sense of purpose, I am excited to embrace the unknown, ready to tackle new challenges, seize opportunities, and continue nurturing the relationships that enrich my life.

In conclusion, 2023 has been a year of profound transformation, filled with moments of joy, sorrow, learning, and growth. It has shown me that every end is a new beginning, and as we close this chapter, we do so with hope, faith, and a heart open to the endless possibilities that await us in the coming year. Here's to embracing the journey ahead with courage, kindness, and an unwavering spirit.`, " "))
)
