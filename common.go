package gobase

import (
	"bytes"
	"fmt"
	"log"
	"runtime"
	"strconv"
)

var (
	GoFunCatchException bool = true
	OnCatchAllPanic     func(err interface{}, args ...interface{})
)

type LogLevel int8

const (
	LogDebug = iota + 1
	LogInfo
	LogWarning
	LogError
	LogSilent = 127
)

// 日志输出接口
type Logger interface {
	LogPrintf(lvl LogLevel, s string, args ...interface{})
}

type SysLogger struct {
	sender  string
	showlvl LogLevel
	skiplvl int
}

func (this *SysLogger) SetShowLvl(minlvl LogLevel) {
	this.showlvl = minlvl
}

func (this *SysLogger) LogPrintf(lvl LogLevel, s string, args ...interface{}) {
	if lvl < this.showlvl {
		return
	}
	logOutput(this.skiplvl, int8(lvl), this.sender, fmt.Sprintf(s, args...))
}

func LogPrintf(s string, args ...interface{}) {
	logOutput(2, 0, "", fmt.Sprintf(s, args...))
}

func SetLogOutput(fn func(calldepth int, lvl int8, sender string, s string)) {
	if fn == nil {
		logOutput = defaultLogFunc
	} else {
		logOutput = fn
	}
}

var defaultLogFunc = func(calldepth int, lvl int8, sender string, s string) {
	log.Output(calldepth+1, s)
}

var logOutput = defaultLogFunc

func DeferCatchPanic(args ...interface{}) {
	if err := recover(); err != nil {
		if OnCatchAllPanic != nil {
			OnCatchAllPanic(err, args)
		}
	}
}

func GetCurrentGoRoutineID() uint64 {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64)
	return n
}

var (
	DefaultLogger = &SysLogger{sender: "", skiplvl: 2}
)
