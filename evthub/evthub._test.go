package evthub

import (
	"sync"
	"testing"
	"time"
)

// 使用方式1: A对象: 创建EvtNode, 执行事件, 释放EvtNode, (B/C/D)对象: 绑定事件 -> 解绑事件
func TestEvtHubUsage1(t *testing.T) {
	cnt := 0

	var invodefunc = func(done func()) {
		node, err := DefaultEvtHub.CreateEvtNode("001")
		if err != nil {
			t.Fatal("err")
		}

		time.Sleep(time.Second)

		if node.Execute(1) != 1 {
			t.Error("err")
		}

		if node.Execute(0) != 2 {
			t.Error("err")
		}

		defer func() {
			DefaultEvtHub.ReleasesEvtNode("001")

			done()
		}()
	}

	var wg sync.WaitGroup

	wg.Add(1)
	go invodefunc(func() {
		wg.Done()
	})

	time.Sleep(100)

	{
		cmdid0 := DefaultEvtHub.AddEvent("001", func(args ...interface{}) (ok bool) {
			cnt++
			return true
		})

		if cmdid0 == -1 {
			t.Fatal("add err")
		}

		defer func() {
			if !DefaultEvtHub.DelEvent("001", cmdid0) {
				t.Fatal("remove err")
			}
		}()
	}

	{
		cmdid0 := DefaultEvtHub.AddEvent("001", func(args ...interface{}) (ok bool) {
			cnt++
			return true
		})

		if cmdid0 == -1 {
			t.Fatal("add err")
		}

		defer func() {
			if !DefaultEvtHub.DelEvent("001", cmdid0) {
				t.Fatal("remove err")
			}
		}()
	}

	wg.Wait()

}

// 使用方式2: (A/B/C/D): CheckGetEvtNode('001', true)获取节点, (node.绑定事件 -> node.解绑事件) / (执行事件) -> 释放EvtNode,
func TestEvtHubUsage2(t *testing.T) {
	cnt := 0
	node := DefaultEvtHub.CheckGetEvtNode("001", true)
	defer func() {
		if !DefaultEvtHub.ReleasesEvtNode("001") {
			t.Fatal("err")
		}
	}()

	{
		cmdid0 := node.Add2Evt(func(args ...interface{}) (ok bool) {
			cnt++
			return true
		})

		defer func() {
			if !node.RemoveEvt(cmdid0) {
				t.Fatal("remove err")
			}
		}()
	}

	{
		cmdid1 := node.Add2Evt(func(args ...interface{}) (ok bool) {
			cnt++
			return true
		})

		defer func() {
			if !node.RemoveEvt(cmdid1) {
				t.Fatal("remove err")
			}
		}()
	}

	if node.Execute(1) != 1 {
		t.Fatal("err")
	}

	if node.Execute(0) != 2 {
		t.Fatal("err")
	}

}

func TestEvtHubUsageBuf(t *testing.T) {

	var buf []byte

	cnt := 0
	node := DefaultEvtHub.CheckGetEvtNode("001", true)
	defer func() {
		if !DefaultEvtHub.ReleasesEvtNode("001") {
			t.Fatal("err")
		}
	}()

	{
		cmdid0 := node.Add2Evt(func(args ...interface{}) (ok bool) {
			bufPtr := args[0].(*[]byte)
			*bufPtr = []byte("abc")
			cnt++
			return true
		})

		defer func() {
			if !node.RemoveEvt(cmdid0) {
				t.Fatal("remove err")
			}
		}()
	}

	{
		cmdid1 := node.Add2Evt(func(args ...interface{}) (ok bool) {
			cnt++
			return true
		})

		defer func() {
			if !node.RemoveEvt(cmdid1) {
				t.Fatal("remove err")
			}
		}()
	}

	if node.Execute(1, &buf) != 1 {
		t.Fatal("err")
	}

	if string(buf) != "abc" {
		t.Fatal("err")
	}

	if node.Execute(0, &buf) != 2 {
		t.Fatal("err")
	}
}
