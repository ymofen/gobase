package binpack

import (
	"fmt"
	"gitee.com/ymofen/gobase"
	"io"
)

const (
	// key  max:255
	// data 0xFFFF max:65535
	// 包协议, minL: 8=1+2+2+3, bodystart:5
	// pre[1]+[0x80]keyl1+[0x80]keyl2 +dataL(LE:2)+data[key+data]+crc24[3]

	BinPackStr_MinL              = 8
	BinPackStr_DataStartIdx      = 5
	BinPackStr_PRE0         byte = 0xF2
)

func WriteBinPackStr(w io.Writer, key string, data []byte) (err error) {
	w.Write([]byte{BinPackStr_PRE0})

	l1 := len(key)
	l2 := len(data)
	if l1 > 0x3FFF {
		return fmt.Errorf("key must less than %d", 0x3FFF)
	}

	if l2 > 0xFFFF {
		return fmt.Errorf("data size must less than %d", 0xFFFF)
	}

	crc := uint32(0)

	l1_1 := (l1 & 0x3F80) >> 7 // 高7位
	l1_0 := l1 & 0x007F        // 低7位

	buf := []byte{byte(0x80 | l1_1), byte(0x80 | l1_0), byte(l2 >> 8), byte(l2)}
	w.Write(buf)
	crc = gobase.CRC24QContinue(crc, buf...)

	buf = []byte(key)
	w.Write(buf)
	crc = gobase.CRC24QContinue(crc, buf...)

	w.Write(data)
	crc = gobase.CRC24QContinue(crc, data...)

	// crc24
	w.Write([]byte{byte(crc >> 16), byte(crc >> 8), byte(crc)})
	return nil
}

type BinPackStrDecode struct {
	decodestep int8

	keyl   int
	datal  int
	cachel int
	msgl   int
	cache  gobase.BytesBuilder
}

func (this *BinPackStrDecode) ExtractMsg() (key string, data []byte) {
	if this.keyl > 0 {
		key = string(this.cache.Buff(BinPack_DataStartIdx, this.keyl))
	}
	if this.datal > 0 {
		data = this.cache.Buff(BinPack_DataStartIdx+this.keyl, this.datal)
	}
	return
}

// 0: need more
// 1: ok
// -1: crc
// -2: errdata
func (this *BinPackStrDecode) InputByte(v byte) int {
	if this.decodestep < 0 {
		this.cache.Reset()
		this.keyl = 0
		this.cachel = 0
		this.datal = 0
		this.msgl = BinPackStr_MinL
		this.decodestep = 1
	}

	this.cache.WriteByte(v)
	this.cachel++
	if this.cachel > BinPackStr_DataStartIdx {
		if this.cachel < this.msgl {
			return 0
		} else {
			crc0 := this.cache.Uint32_BE(this.cachel - 4)
			crc0 = crc0 & 0x00FFFFFF
			crc1 := gobase.CRC24QBuf(this.cache.BuffEx(1, this.cachel-3))
			this.decodestep = -1
			if crc1 != crc0 {
				return -1
			}
			return 1
		}
	}
	if this.cachel == 1 {
		if v != BinPackStr_PRE0 {
			this.decodestep = -1
			return -2
		}
	} else if this.cachel == 2 {
		if v&0x80 != 0x80 {
			this.decodestep = -1
			return -2
		}
		this.keyl = int(v) & 0x7F << 7
	} else if this.cachel == 3 {
		if v&0x80 != 0x80 {
			this.decodestep = -1
			return -2
		}
		this.keyl += int(v & 0x7F)
	} else if this.cachel == 5 {
		this.datal = int(this.cache.Uint16_BE(3))
		this.msgl = BinPackStr_MinL + this.keyl + this.datal
	}

	return 0
}
