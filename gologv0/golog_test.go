package gologv0_test

import (
	"fmt"
	"gitee.com/ymofen/gobase"
	"gitee.com/ymofen/gobase/golog"
	"log"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestGolog(t *testing.T) {
	gologv0.AddLogger("testlog", "*", func(rec *gologv0.GoLogRecord) bool {
		t.Logf("%v(%s)", rec.Msg, rec.Source)
		return true
	})
	defer gologv0.DelLogger("testlog", "*")

	gologv0.LogMsgf("hello", 0, 0, "hello %s", gobase.RandKeyString(24))

}

func TestGolog2(t *testing.T) {
	gologv0.AddLogger("console", "*", gologv0.ConsoleLogMsg)
	defer gologv0.DelLogger("console", "*")

	gologv0.LogMsgf("TEST", 0, 0, "hello %s", gobase.RandKeyString(24))
	gologv0.LogMsgf("TEST", 0, 0, "hello %s", gobase.RandKeyString(24))

	time.Sleep(time.Millisecond)
}

func TestGologAddonsSingleCacheFileMsgf(t *testing.T) {
	gobase.GoFunCatchException = false
	filelog := gologv0.NewSingleCacheFileWriter("log/app.log")
	//  golog_test.go:43: 100W, 6685(ms)
	filelog.SetMaxSize(1024 * 1024 * 10)

	defer filelog.Close()

	cnt := 1000000
	datalst := make([]string, 1024)
	for i := 0; i < len(datalst); i++ {
		datalst[i] = gobase.RandKeyString(1024)
	}

	t0 := time.Now()
	for i := 0; i < cnt; i++ {
		filelog.WriteMsgf(0, "hello %s\r\n", datalst[i%len(datalst)])
	}
	filelog.WaitIdle()
	ms := time.Since(t0).Milliseconds()
	t.Logf("%d, %d/s", cnt, int64(cnt)*1000/ms)

	time.Sleep(time.Second * 0)

}

func TestGologAddonsSingleCacheFile(t *testing.T) {
	gobase.GoFunCatchException = false
	filelog := gologv0.NewSingleCacheFileWriter("log/app.log")
	filelog.SetMsgFmt("%P:%L %u %M (%s)\r\n")

	// 1:golog_test.go:101: 1000001, 534759/s
	// 10:golog_test.go:100: 1000010, 1250012/s
	filelog.SetMaxSize(1024 * 1024 * 50)
	defer filelog.Close()
	gologv0.AddLogger("applog", "*", filelog.OnLogMsg)
	defer gologv0.DelLogger("applog", "*")

	var maxN int32 = 1000000
	datalst := make([]string, 1024)
	for i := 0; i < len(datalst); i++ {
		datalst[i] = gobase.RandKeyString(1024)
	}

	var cnt int32 = 0
	var wg sync.WaitGroup
	t0 := time.Now()
	// 10线程
	for j := 0; j <= 1; j++ {
		wg.Add(1)
		go func() {
			for {
				idx := atomic.AddInt32(&cnt, 1)
				if idx < maxN {
					filelog.WriteMsgf(0, "hello %s\r\n", datalst[int(idx)%len(datalst)])
				} else {
					filelog.WriteMsgf(0, "hello %s\r\n", datalst[int(idx)%len(datalst)])
					break
				}
			}
			wg.Done()
		}()
	}
	wg.Wait()
	filelog.WaitIdle() // 等待都写入到文件
	ms := time.Since(t0).Milliseconds()
	t.Logf("%d, %d/s", cnt, int64(cnt)*1000/ms)

	time.Sleep(time.Second * 0)

}

// 系统自带
func TestLog(t *testing.T) {
	gobase.GoFunCatchException = false
	//filelog := golog.NewSingleCacheFileWriter("log/app.log")
	//filelog.SetMsgFmt("%P:%L %u %M (%s)\r\n")

	// golog_test.go:112: 1000000, 210703/s
	//logfile, err := os.OpenFile("app.log", os.O_CREATE|os.O_TRUNC, 0666)
	//if err != nil {
	//	t.Fatal(err)
	//	return
	//}

	//1:golog_test.go:154: 1000001, 232829/s
	//10: golog_test.go:154: 1000010, 223715/s
	logfile := gobase.NewCacheFile("app.log", 1024)

	log.SetFlags(log.Lshortfile)
	log.SetOutput(logfile)

	var maxN int32 = 1000000
	datalst := make([]string, 1024)
	for i := 0; i < len(datalst); i++ {
		datalst[i] = gobase.RandKeyString(1024)
	}

	var cnt int32 = 0
	var wg sync.WaitGroup
	t0 := time.Now()
	// 10线程
	for j := 0; j <= 10; j++ {
		wg.Add(1)
		go func() {
			for {
				idx := atomic.AddInt32(&cnt, 1)
				if idx < maxN {
					log.Printf("hello %s\r\n", datalst[int(idx)%len(datalst)])
				} else {
					log.Printf("hello %s\r\n", datalst[int(idx)%len(datalst)])
					break
				}
			}
			wg.Done()
		}()

	}
	wg.Wait()
	ms := time.Since(t0).Milliseconds()
	t.Logf("%d, %d/s", cnt, int64(cnt)*1000/ms)
	time.Sleep(time.Second * 0)

}

func TestGologAddonsCacheFileFlush(t *testing.T) {
	gobase.GoFunCatchException = false
	filelog := gologv0.NewSingleCacheFileWriter("log/app.log")
	filelog.SetMsgFmt("%P:%L %u %M (%s)\r\n")

	//  golog_test.go:43: 100W, 6685(ms)
	filelog.SetMaxSize(1024 * 1024 * 10)
	defer filelog.Close()
	gologv0.AddLogger("applog", "*", filelog.OnLogMsg)
	defer gologv0.DelLogger("applog", "*")

	gologv0.LogMsgf("TEST", 0, 0, "hello %s", gobase.RandKeyString(24))

	time.Sleep(time.Second * 35)
}

func TestGologAddonsMultiCacheFile(t *testing.T) {
	gobase.GoFunCatchException = false
	filelog := gologv0.NewCacheMultiFileWriter("log/app.log")
	filelog.SetMsgFmt("%P:%L %u %M (%s)\r\n")

	//  golog_test.go:43: 100W, 6685(ms)
	filelog.SetMaxSize(1024 * 1024)
	defer filelog.Close()
	gologv0.AddLogger("applog", "*", filelog.OnLogMsg)
	defer gologv0.DelLogger("applog", "*")

	t0 := time.Now()
	for i := 0; i < 1000000; i++ {
		gologv0.LogMsgf(fmt.Sprintf("TEST-%d", i%10), 0, 0, "hello %s", gobase.RandKeyString(24))
	}
	filelog.WaitIdle()
	t.Logf("%d(ms)", time.Since(t0).Milliseconds())

	time.Sleep(time.Second * 0)

}
