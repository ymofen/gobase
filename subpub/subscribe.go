package subpub

import (
	"fmt"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

/*
支持使用路由进行订阅 # 匹配下级所有， +匹配当前层
topic/news

10000 topic

pub:
sync.map
lk:
	utils_subscribe_v2._test.go:168: 25505, 5094/s
	utils_subscribe_v2._test.go:168: 50409, 5035/s

nolk:
	utils_subscribe_v2._test.go:168: 25767, 5135/s
	utils_subscribe_v2._test.go:168: 51914, 5175/s

map with lk
		utils_subscribe_v2._test.go:168: 34228, 6844/s
		utils_subscribe_v2._test.go:168: 68952, 6894/s

map with lk
	    utils_subscribe_v2._test.go:168: 35343, 6977/s
	    utils_subscribe_v2._test.go:168: 71194, 7072/s

sesslst -> map
    utils_subscribe_v2._test.go:168: 36668, 7321/s
    utils_subscribe_v2._test.go:168: 74022, 7383/s
*/

type SubFunc = func(id, topic string, args ...interface{}) (ok bool)

type ISub interface {
	Sub(id, topic string, cb func(id, topic string, args ...interface{}) (ok bool))
}

type ISubRA interface {
	SubR(id, topic string, cb SubFunc) (r int)
	UnsubR(id, topic string) (r int)
}

type ISubA interface {
	ISub
	IUnsub
}

type IUnsub interface {
	Unsub(id, topic string) bool
}

type IPub interface {
	Pub(topic string, max int, args ...interface{}) int
}

type ISubPub interface {
	ISub
	IUnsub
	IPub
}

type IStatus interface {
	Status() string
}

type channelSubSession struct {
	refN int32
	id   string
	fn   SubFunc
}

// 发布通道
type channelItem struct {
	id            string
	matchTopic    interface{}
	sesslst       map[string]*channelSubSession
	idlst         []string
	fnlst         []SubFunc
	lastActivityT int64
}

// 订阅主题
type subTopicItem struct {
	subtopicid string
	subtopic   interface{}
	sesslst    map[string]SubFunc
}

// 订阅中心,
// 订阅主题:支持通配符/news/+/qq, 将会收到匹配的的通道消息
// PUB消息通道不支持通配符
type Subscribe struct {
	lockcnt int32

	lk sync.RWMutex
	// 通道列表
	channellst map[string]*channelItem

	// 订阅主题列表
	subTopiclst    map[string]*subTopicItem
	routeParseFunc func(topic string) interface{}                  // 必须确保有值
	routeMatchFunc func(route interface{}, topic interface{}) bool // 必须确保有值
}

var (
	DefaultSubscribe = NewSubscribe()
)

func innerParseRoute(route string) interface{} {
	return strings.Split(route, "/")
}

func innerRouteIncludesTopic0(route []string, topic []string) bool {
	if len(route) == 0 {
		return len(topic) == 0
	}

	if len(topic) == 0 {
		return route[0] == "#"
	}

	if route[0] == "#" {
		return true
	}

	if (route[0] == "+") || (route[0] == topic[0]) {
		return innerRouteIncludesTopic0(route[1:], topic[1:])
	}
	return false
}

/*
1000W:consume:730(ms)
*/
func innerRouteIncludesTopic(route interface{}, topic interface{}) bool {
	return innerRouteIncludesTopic0(route.([]string), topic.([]string))
}

func NewSubscribe() *Subscribe {
	return &Subscribe{
		channellst:     make(map[string]*channelItem),
		subTopiclst:    make(map[string]*subTopicItem),
		routeParseFunc: innerParseRoute,
		routeMatchFunc: innerRouteIncludesTopic,
	}
}

func NewSubscribeEx(routeParseFunc func(topic string) interface{}, routeMatchFunc func(route interface{}, topic interface{}) bool) *Subscribe {
	if routeParseFunc == nil {
		panic("invalid routeParseFunc")
	}
	if routeMatchFunc == nil {
		panic("invalid routeMatch")
	}
	return &Subscribe{
		channellst:     make(map[string]*channelItem),
		subTopiclst:    make(map[string]*subTopicItem),
		routeParseFunc: routeParseFunc,
		routeMatchFunc: routeMatchFunc,
	}
}

func (this *Subscribe) Close() error {
	this.lk.Lock()
	defer this.lk.Unlock()
	for k, _ := range this.subTopiclst {
		delete(this.subTopiclst, k)
	}
	return nil
}

func (this *Subscribe) matchSubTopic(topics interface{}, fn func(itm *subTopicItem) bool) {
	this.rangeSubTopics(func(key string, itm *subTopicItem) bool {
		if this.routeMatchFunc(itm.subtopic, topics) {
			return fn(itm)
		}
		return true
	})
}

func (this *Subscribe) checkGetTopic(topic string, new bool) *subTopicItem {
	itm := this.subTopiclst[topic]
	if new && itm == nil {
		itm = &subTopicItem{subtopicid: topic, subtopic: this.routeParseFunc(topic), sesslst: make(map[string]SubFunc)}
		this.subTopiclst[topic] = itm

	}
	return itm
}

func (this *Subscribe) Status() string {
	return fmt.Sprintf("subtopic-n:%d, channel:%d, sess:%d", this.TopicCount(), len(this.channellst), this.Count())
}

func (this *Subscribe) GetTopicSubCount(topic string) int {
	this.lk.RLock()
	defer this.lk.RUnlock()
	itm := this.checkGetTopic(topic, false)
	if itm == nil {
		return 0
	}
	return len(itm.sesslst)
}

func (this *Subscribe) TopicCount() int {
	this.lk.RLock()
	defer this.lk.RUnlock()
	return len(this.subTopiclst)
}

func (this *Subscribe) rangeSubTopics(fn func(key string, itm *subTopicItem) bool) {
	for k, v := range this.subTopiclst {
		if !fn(k, v) {
			break
		}
	}
}

func (this *Subscribe) Count() int {
	this.lk.RLock()
	defer this.lk.RUnlock()
	n := 0
	this.rangeSubTopics(func(key string, itm *subTopicItem) bool {
		n += len(itm.sesslst)
		return true
	})
	return n
}

func (this *Subscribe) StatusDetail() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("topic:%d", this.TopicCount()))
	lst := make([]string, 0, 1024)
	this.rangeSubTopics(func(key string, itm *subTopicItem) bool {
		lst = append(lst, key)
		return true
	})
	sort.Strings(lst)
	for i := 0; i < len(lst); i++ {
		itm := this.checkGetTopic(lst[i], false)
		if itm != nil {
			sb.WriteString(fmt.Sprintf(",%s:%d", lst[i], len(itm.sesslst)))
		}
	}
	return sb.String()
}

func (this *Subscribe) add2Topic(channel *channelItem, subid string, cb SubFunc) (changed bool) {
	itm := channel.sesslst[subid]
	if itm == nil {
		itm = &channelSubSession{id: subid, fn: cb}
		channel.sesslst[subid] = itm
		changed = true
	}
	itm.refN++
	return
}

func (this *Subscribe) releaseTopic(topic *channelItem, subid string) (changed bool) {
	itm := topic.sesslst[subid]
	if itm == nil {
		return false
	}
	itm.refN--
	if itm.refN == 0 {
		delete(topic.sesslst, subid)
		return true
	}
	return false
}

func (this *Subscribe) innerAdd2Topic(id string, topic interface{}, cb SubFunc) {
	for _, v := range this.channellst {
		if this.routeMatchFunc(topic, v.matchTopic) {
			if this.add2Topic(v, id, cb) {
				this.innerReloadSubSessionFnlst(v)
			}
		}
	}
}

func (this *Subscribe) innerRemoveFromTopic(id string, topic interface{}) (cnt int) {
	for _, v := range this.channellst {
		if this.routeMatchFunc(topic, v.matchTopic) {
			if this.releaseTopic(v, id) {
				cnt++
				this.innerReloadSubSessionFnlst(v)
			}
		}
	}
	return
}

func (this *Subscribe) innerCheckCreateChannel(channel string) *channelItem {
	itm := this.channellst[channel]
	if itm != nil {
		return itm
	}
	itm = &channelItem{matchTopic: this.routeParseFunc(channel), id: channel, sesslst: make(map[string]*channelSubSession)}
	this.channellst[channel] = itm
	this.innerCollectSubSession(itm)
	return itm
}

func (this *Subscribe) innerReloadSubSessionFnlst(itm *channelItem) {
	fnlst := make([]SubFunc, len(itm.sesslst))
	idlst := make([]string, len(itm.sesslst))

	i := 0
	for _, s := range itm.sesslst { // 所有session都添加进去
		if s.fn == nil {
			panic("xxx")
		}
		fnlst[i] = s.fn
		idlst[i] = s.id
		i++
	}

	itm.fnlst, itm.idlst = fnlst, idlst
}

func (this *Subscribe) innerCollectSubSession(itm *channelItem) {
	for _, v := range this.subTopiclst {
		if this.routeMatchFunc(v.subtopic, itm.matchTopic) {
			for sid, sfn := range v.sesslst { // 所有session都添加进去
				this.add2Topic(itm, sid, sfn)
			}
		}
	}
	this.innerReloadSubSessionFnlst(itm)
}

// id/channel 不能为空
func (this *Subscribe) SubR(id, topic string, cb SubFunc) (r int) {
	this.lk.Lock()
	defer this.lk.Unlock()
	itm := this.checkGetTopic(topic, true)
	itm.sesslst[id] = cb
	this.innerAdd2Topic(id, itm.subtopic, cb)
	return len(itm.sesslst)
}

// 订阅一个主题
//
// topic订阅主题,为空不进行订阅
// id订阅者id,topic 下id重复将会被覆盖(之前订阅失效)
func (this *Subscribe) Sub(id, topic string, cb SubFunc) {
	if len(topic) == 0 {
		return
	}
	this.lk.Lock()
	defer this.lk.Unlock()
	itm := this.checkGetTopic(topic, true)
	itm.sesslst[id] = cb
	this.innerAdd2Topic(id, itm.subtopic, cb)
	return
}

// id/channel can't be empty
// r: -1 channel 不存在, >=0 channel 订阅数量
func (this *Subscribe) UnsubR(id, topic string) (r int) {
	this.lk.Lock()
	defer this.lk.Unlock()
	atomic.AddInt32(&this.lockcnt, 1)
	itm := this.checkGetTopic(topic, false)
	if itm != nil {
		delete(itm.sesslst, id)
		r = len(itm.sesslst)
		if r == 0 {
			delete(this.subTopiclst, topic)
		}
		this.innerRemoveFromTopic(id, itm.subtopic)
		return r
	}
	return -1
}

// 取消订阅
// id 订阅时传入的id
// topic订阅的主题
func (this *Subscribe) Unsub(id, topic string) bool {
	if len(topic) == 0 {
		return false
	}
	this.lk.Lock()
	defer this.lk.Unlock()
	atomic.AddInt32(&this.lockcnt, 1)
	itm := this.checkGetTopic(topic, false)
	if itm != nil {
		delete(itm.sesslst, id)
		if len(itm.sesslst) == 0 {
			delete(this.subTopiclst, topic)
		}
		this.innerRemoveFromTopic(id, itm.subtopic)
		return true
	}
	return false
}

// >1 投递成功max次后停止
// 循环map
//
//	utils_subscribe_v2._test.go:168: 34228, 6844/s
//	utils_subscribe_v2._test.go:168: 68952, 6894/s
//
// 循环lst
//
//	utils_subscribe_v2._test.go:168: 35343, 6977/s
//	utils_subscribe_v2._test.go:168: 71194, 7072/s
//
// 向主题订阅者推送数据
// topic推送的主题
// max最大接收者,超过该值不再进行推送
//
//max:0, 全部投递
//max:0, 全部投递
func (this *Subscribe) Pub(channel string, max int, args ...interface{}) int {
	n := 0
	var fnlst []SubFunc
	var idlst []string
	this.lk.RLock()
	itm := this.channellst[channel]
	if itm != nil {
		fnlst, idlst = itm.fnlst, itm.idlst
		itm.lastActivityT = time.Now().Unix()
	}
	this.lk.RUnlock()
	if itm == nil {
		this.lk.Lock()
		atomic.AddInt32(&this.lockcnt, 1)
		itm = this.innerCheckCreateChannel(channel)
		fnlst, idlst = itm.fnlst, itm.idlst
		itm.lastActivityT = time.Now().Unix()
		this.lk.Unlock()
	}

	if len(idlst) != len(fnlst) {
		return -1
	}

	for idx, fn := range fnlst {
		if fn(idlst[idx], channel, args...) {
			n++
			if max > 0 && n >= max {
				break
			}
		}
	}
	return n
}

func (this *Subscribe) innerFreeChannel(itm *channelItem) bool {
	if itm == nil || len(itm.sesslst) > 0 {
		return false
	}
	delete(this.channellst, itm.id)
	itm.matchTopic = nil
	itm.idlst = nil
	itm.sesslst = nil
	itm.fnlst = nil
	itm.idlst = nil
	return true
}

// 释放通道
func (this *Subscribe) ClosePubChannel(channel string) (closed bool) {
	this.lk.Lock()
	defer this.lk.Unlock()

	atomic.AddInt32(&this.lockcnt, 1)
	itm := this.channellst[channel]
	return this.innerFreeChannel(itm)
}

// 清理一些超时10分钟没有发布消息的通道
func (this *Subscribe) CleanChannels() (cnt int) {
	t := time.Now().Unix()
	var lst []*channelItem
	this.lk.RLock()
	for _, itm := range this.channellst {
		if t-itm.lastActivityT > 600 { // 10分钟没有发布数据, 进行清理
			lst = append(lst, itm)
		}
	}
	lst = append(lst)
	this.lk.RUnlock()

	if len(lst) > 0 {
		this.lk.Lock()
		defer this.lk.Unlock()
		for i := 0; i < len(lst); i++ {
			if this.innerFreeChannel(lst[i]) {
				cnt++
			}
		}
	}
	return

}
