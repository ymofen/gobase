package msgpack

import (
	"bytes"
	"encoding/binary"
	"gitee.com/ymofen/gobase"
	"math/rand"
	"testing"
	"time"
)

func TestWriteL(t *testing.T) {
	buf := make([]byte, 4)
	binary.BigEndian.PutUint32(buf, 0xAABBCCDD)
	t.Logf("%s", gobase.BufToHexStr(buf, 0, ""))

	var sb gobase.BytesBuilder
	WriteMsgPackLen(&sb, msgpack_str_0xD9, 0xAABB, 2)
	if sb.HexString("") != "D9AABB" {
		t.Fatalf("writel err")
	}

	sb.Cleanup()
	WriteMsgPackLen(&sb, msgpack_str_0xD9, 0xAABBCCDD, 4)
	if sb.HexString("") != "D9AABBCCDD" {
		t.Fatalf("writel err%s", sb.HexString(""))
	}
}

func TestMsgPackStrEncDec(t *testing.T) {
	var bb gobase.BytesBuilder
	strs0 := make([]string, 0)
	for i := 0; i < 10; i++ {
		str0 := gobase.RandKeyString(rand.Intn(0xFFFFFF))
		strs0 = append(strs0, str0)
		WriteMsgPackStr(&bb, str0)
	}

	strs := make([]string, 0)
	msgpackDec := NewMsgPackDecode()
	for i := 0; i < bb.Len(); i++ {
		r := msgpackDec.InputByte(bb.Byte(i))
		if r == 1 {
			strs = append(strs, string(msgpackDec.Bytes()))
		}
	}

	if len(strs) != len(strs0) {
		t.Fatalf("dec err")
	} else {
		for i := 0; i < len(strs); i++ {
			if strs[i] != strs0[i] {
				t.Fatalf("%d:dec err \n%s\n%s", i, strs0[i], strs[i])
			}
		}
	}
}

func TestMsgPackBinEncDec(t *testing.T) {
	var bb gobase.BytesBuilder
	strs0 := make([][]byte, 0)
	for i := 0; i < 10; i++ {
		str0 := gobase.RandBuf(rand.Intn(0xFFFFFF))
		strs0 = append(strs0, str0)
		WriteMsgPackBin(&bb, str0)
	}

	strs := make([][]byte, 0)
	msgpackDec := NewMsgPackDecode()
	for i := 0; i < bb.Len(); i++ {
		r := msgpackDec.InputByte(bb.Byte(i))
		if r == 1 {
			strs = append(strs, gobase.CloneBytes(msgpackDec.Bytes(), 0, 0))
		}
	}

	bb.Reset()

	if len(strs) != len(strs0) {
		t.Fatalf("dec err")
	} else {
		for i := 0; i < len(strs); i++ {
			if bytes.Compare(strs[i], strs0[i]) != 0 {
				t.Fatalf("%d:dec err \n%s\n%s", i, gobase.BufToHexStr(strs0[i], 0, ""), gobase.BufToHexStr(strs[i], 0, ""))
			}
		}
	}

}

func TestMsgPackBinEncDecSpped(t *testing.T) {
	buflst := make([][]byte, 1024)
	for i := 0; i < len(buflst); i++ {
		buflst[i] = gobase.RandBuf(rand.Intn(2048))
	}

	t0 := time.Now()
	n := 0
	msgpackDec := NewMsgPackDecode()
	for i := 0; i < 1000000; i++ {
		buf := buflst[rand.Intn(7777)%len(buflst)]
		var bb gobase.BytesBuilder
		WriteMsgPackBin(&bb, buf)

		for i := 0; i < bb.Len(); i++ {
			r := msgpackDec.InputByte(bb.Byte(i))
			if r == 1 {
				if bytes.Compare(buf, msgpackDec.Bytes()) != 0 {
					t.Fatalf("dec err not match")
				}
			} else if r == -1 {
				t.Fatalf("err decode")
			}
		}
		n++
	}

	t.Logf("%d, speed:%d/s", n, (int64(n)/time.Since(t0).Milliseconds())*1000)

}
