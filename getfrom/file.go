package getfrom

import "os"

func init() {
	Register("file", func(args ...interface{}) (data []byte, err error) {
		return os.ReadFile(args[0].(string))
	})
}
