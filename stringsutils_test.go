package gobase_test

import (
	"fmt"
	"gitee.com/ymofen/gobase"
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

func TestKv(t *testing.T) {
	strMap := gobase.NewStrMap()
	strMap["a"] = "1"
	strMap["b"] = "2"
	strMap["c"] = ""
	strMap["d"] = "0"

	s := strMap.SortRangeUrlEncode(func(k, v string) bool {
		return true
	})

	require.Equal(t, s, strMap.URLFormEncodeKeylst("a", "b", "c", "d"))

	t.Logf("%s", s)
}

func BenchmarkMergeSets(b *testing.B) {
	set1 := strings.Split("a,b,a,c", ",")
	set2 := strings.Split("d,c,e", ",")
	excepted := []string{"a", "b", "c", "d", "e"}
	for i := 0; i < b.N; i++ {
		vlst := gobase.MergeSets(set1, set2)
		require.Equal(b, excepted[:], vlst)
	}
}

func BenchmarkMergeSets2(b *testing.B) {
	set1 := strings.Split("a,b,a,c", ",")
	set2 := strings.Split("d,c,e", ",")
	excepted := []string{"a", "b", "c", "d", "e"}
	for i := 0; i < b.N; i++ {
		vlst := gobase.MergeSets2(set1, set2)
		require.Equal(b, excepted[:], vlst)
	}
}

// BenchmarkStrToIntListEx-16    	 1286896	       938.9 ns/op
func BenchmarkStrToIntListEx(b *testing.B) {
	str := "1,,3"
	excepted := [3]uint8{1, 255, 3}
	for i := 0; i < b.N; i++ {
		vlst := gobase.StrToIntListEx[uint8](str, ",", true, 255)
		require.Equal(b, excepted[:], vlst)
	}
}

// BenchmarkStrToFloatList-16    	  787726	      1385 ns/op
func BenchmarkStrToFloatList(b *testing.B) {
	str := "1,2,3"
	excepted := [3]float64{1, 2, 3}
	for i := 0; i < b.N; i++ {
		vlst := gobase.StrToFloatList(str, ",", 0)
		require.Equal(b, excepted[:], vlst)
	}
}

// BenchmarkStrToFloatListEx-16    	  847912	      1385 ns/op
func BenchmarkStrToFloatListEx(b *testing.B) {
	str := "1,2,3"
	excepted := [3]float64{1, 2, 3}
	for i := 0; i < b.N; i++ {
		vlst := gobase.StrToFloatListEx[float64](str, ",", false, 0)
		require.Equal(b, excepted[:], vlst)
	}
}

func BenchmarkStrToFloatList2(b *testing.B) {
	str := "1,,3"
	// excepted := [3]float64{1, 9999, 3}
	excepted := gobase.StrToFloatListEx[float64](str, ",", true, 9999)

	for i := 0; i < b.N; i++ {
		vlst := gobase.StrToFloatList(str, ",", 9999)
		require.Equal(b, excepted, vlst)
	}
}

func TestStrMapURL(t *testing.T) {
	strMap := gobase.NewStrMap()

	strMap.URLFormDecode("requestType=vrs&sub.conntype=ntripcli&sub.interval=0&sub.connstr=&connstr=yangmf:123456@119.96.176.50:8023&sub.mp=RTCM33GRCEJpro&request.data={{gga}}&键=中国&标点=\"")
	require.Equal(t, "{{gga}}", strMap.StringByName("request.data", ""))
	strMap["data"] = fmt.Sprintf("logo=abc&region=113\r\n38.4\"&键=中国")

	strMap2 := gobase.NewStrMap()
	s := strMap.URLEncode0()

	t.Logf("%s", s)
	strMap2.URLFormDecode(s)
	for k, v := range strMap {
		require.Equal(t, v, strMap2[k])
	}

	strMap2.Reset()
	s0 := "request.data={{gga}}"
	strMap2.URLFormDecode(s0)
	s = strMap2.URLEncode0()
	require.Equal(t, s0, s)

}

func TestSplit(t *testing.T) {
	testcases := []struct {
		input  string
		sep    string
		output [2]string
	}{
		{input: "abc-defg", sep: "-", output: [2]string{"abc", "defg"}},
		{input: "abc--defg", sep: "--", output: [2]string{"abc", "defg"}},
		{input: "中国--湖南--长沙", sep: "--", output: [2]string{"中国", "湖南--长沙"}},
	}
	for _, tc := range testcases {
		s1, s2 := gobase.Split2Str(tc.input, tc.sep)
		require.Equal(t, s1, tc.output[0])
		require.Equal(t, s2, tc.output[1])
	}
}

func TestLastSplit(t *testing.T) {
	testcases := []struct {
		input  string
		sep    string
		output [2]string
	}{
		{input: "abc-defg", sep: "-", output: [2]string{"abc", "defg"}},
		{input: "abc--defg", sep: "--", output: [2]string{"abc", "defg"}},
		{input: "中国--湖南--长沙", sep: "--", output: [2]string{"中国--湖南", "长沙"}},
	}
	for _, tc := range testcases {
		s1, s2 := gobase.LastSplit2Str(tc.input, tc.sep)
		require.Equal(t, s1, tc.output[0])
		require.Equal(t, s2, tc.output[1])
	}
}

func TestStrMapUrlEncode0(t *testing.T) {
	strMap := gobase.NewStrMap()

	strMap.URLFormDecode("requestType=vrs&sub.conntype=ntripcli&sub.interval=0&sub.connstr=&connstr=yangmf:123456@119.96.176.50:8023&sub.mp=RTCM33GRCEJpro&request.data={{gga}}&键=中国&标点=\"")
	newMap := gobase.NewStrMap()
	s := strMap.URLEncode0WithKeys("sub.mp", "键", "标点", "request.data", "connstr")
	newMap.URLFormDecode(s)
	for k, v := range newMap {
		require.Equal(t, v, strMap[k])
	}

}

func TestParseToken(t *testing.T) {
	raw := "'-' 3)$"

	tokens := gobase.ParseTokens(raw, ' ')
	require.Equal(t, "3)$", tokens[1])

}

func BenchmarkParseTokens(b *testing.B) {
	raw := `$($env.hostname spidx  ' ' 3)$`
	for i := 0; i < b.N; i++ {
		tokens := gobase.ParseTokens(raw, ' ')
		require.Equal(b, " ", tokens[2])
	}
}

func BenchmarkSplit(b *testing.B) {
	raw := `$($env.hostname spidx - 3)$`
	for i := 0; i < b.N; i++ {
		tokens := strings.Split(raw, " ")
		require.Equal(b, "-", tokens[2])
	}
}
