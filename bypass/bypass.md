# ByPassGroup
组内分流,管理分流节点

## func (this *ByPassGroup) ConfigStrategy(v int8)
组的分流策略
* 0：轮流分流 (遇到不可用时跳过) 默认策略
* 1：依次分流 (从第一个开始分流， 不可用时跳到下一个, 需要配合max)
* 2：分流到较少的节点上(不可用不参与分流)

## func (this *ByPassGroup) BeginUpdate()
* 开始进行信息更新, 与EndUpdate配套使用, 会进行加锁.

## func (this *ByPassGroup) EndUpdate(removeflag bool) (upcnt int)
* removeflag true:清理没有更新过的
* upcnt 本次更新数量

## func (this *ByPassGroup) UpdateByPassNode(id string, sn int16, max int, data interface{}) (isChanged bool)
更新分流节点, 如果节点不存在则进行新增
* id: 节点id, 组内不可以重复
* sn: 组内顺序, sn小的优先进行选择
* max: 最大使用数量, 如果为0则不进行限制
* data: 节点数据

## func (this *ByPassGroup) BlockNode(id string, dura time.Duration) (ok bool)
* 阻塞一段时间

## func (this *ByPassGroup) DeleteNode(id string) (isChanged bool)
* 删除一个节点

## func (this *ByPassGroup) GetByPassNode() (node *ByPassItem)
获取并锁定一个分流节点, 如果获取失败返回nil, 
* 注意:获取成功, 要确保进行归还

## func (this *ByPassGroup) ReleaseByPassNode(node *ByPassItem, fail bool, block time.Duration, failmsg string)
解锁一个节点的使用
* node 需要归还的节点
* fail 表示使用失败
* block 锁定时间(指定时间内不允许被获取)
* failmsg 使用失败的消息

## func (this *ByPassGroup) Status() string
分流状态, 输出各个节点的状态