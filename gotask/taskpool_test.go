package gotask_test

import (
	"gitee.com/ymofen/gobase"
	"gitee.com/ymofen/gobase/gotask"
	"sync/atomic"
	"testing"
	"time"
)

func TestTaskPoolChan(t *testing.T) {
	lst := make(chan interface{}, 0)
	go func() {
		lst <- "1"
		lst <- "2"
	}()

	t.Logf("n:%d", len(lst))
	t.Log(<-lst)
	t.Log(<-lst)
	t.Logf("n:%d", len(lst))
}

func TestTaskPoolUsage(t *testing.T) {
	pl := gotask.NewTaskPool(1024)
	pl.WorkMin = 128
	pl.WorkMax = 256
	pl.RunMinWorkers()
	defer pl.Close()

	pl.PostTask(func(worker *gotask.TaskWorker, args ...interface{}) {
		t.Log(args...)
	}, "a", 1, 2.0, "any")

	time.Sleep(time.Second)

}

func TestTaskPool(t *testing.T) {
	gobase.GoFunCatchException = false
	pl := gotask.NewTaskPool(1024)
	pl.WorkMin = 128
	pl.WorkMax = 256
	pl.RunMinWorkers()
	n := int64(0)
	failn := int64(n)
	t1 := time.Now()
	pl.PostTask(func(worker *gotask.TaskWorker, args ...interface{}) {
		for pl.Terminated == 0 && time.Since(t1).Seconds() < 10 {
			if !pl.PostTask(func(worker *gotask.TaskWorker, args ...interface{}) {
				atomic.AddInt64(&n, 1)
			}) {
				atomic.AddInt64(&failn, 1)
			}
		}
	})

	time.AfterFunc(time.Second*20, func() {
		t.Log("关闭任务池")
		pl.Close()
		t.Log("成功关闭任务池")
	})

	for {
		p := n
		t1 := time.Now()
		time.Sleep(time.Second * 2)
		n1 := n - p
		t.Logf("n:%d, fail-n:%d, worker:%d, task:%d(%d/%d), speed:%.2f/s", n1, failn, pl.WorkNum(), pl.GetTaskNum(), pl.TaskPush, pl.TaskPop, float64(n1)/time.Since(t1).Seconds())
	}
}
