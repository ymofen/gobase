package gobase

import "fmt"

const (
	EMPTY_STRING      string = ""
	QUOTECHR                 = '"'
	CHAR_QUOTE_DOUBLE        = '"'
	CHAR_QUOTE_SINGLE        = '\''
	CHAR_SLASH               = '\\'
)

var (
	ErrUnsupported   = fmt.Errorf("unsupported")
	ErrInvalidObject = fmt.Errorf("invalid object")
)
