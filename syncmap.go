package gobase

import (
	"sort"
	"strings"
	"sync"
)

/**
 * 可以单独使用
 */
type SyncMap struct {
	storeMap sync.Map
}

func (this *SyncMap) Set(id interface{}, val interface{}) {
	this.storeMap.Store(id, val)
}

func (this *SyncMap) Store(id interface{}, val interface{}) {
	this.storeMap.Store(id, val)
}

func (this *SyncMap) Contain(id interface{}) bool {
	_, loaded := this.storeMap.Load(id)
	return loaded
}

func (this *SyncMap) Count() int {
	return SyncMapLength(&this.storeMap)
}

func (this *SyncMap) CountFunc(fn func(k, v interface{}) bool) int {
	rval := 0
	this.storeMap.Range(func(key, value interface{}) bool {
		if fn(key, value) {
			rval++
		}
		return true
	})
	return rval
}

func (this *SyncMap) Clear() {
	//this.storeMap = emptyMap    // 测试时发现 存在并发问题
	this.Range(func(key, value interface{}) bool {
		this.Remove(key)
		return true
	})
}

func (this *SyncMap) Get(id interface{}, def interface{}) interface{} {
	if val, loaded := this.storeMap.Load(id); loaded {
		return val
	} else {
		return def
	}
}

func (this *SyncMap) LoadOrStoreFunc2(id interface{}, newfunc func() interface{},
	freefunc func(val interface{})) (actual interface{}, loaded bool) {
	if val, loaded := this.storeMap.Load(id); loaded {
		return val, true
	} else {
		newval := newfunc()
		val, loaded := this.storeMap.LoadOrStore(id, newval)
		if loaded && freefunc != nil {
			freefunc(newval)
		}
		return val, loaded
	}
}

func (this *SyncMap) LoadOrStoreFunc(id interface{}, newfunc func() interface{},
	freefunc func(val interface{})) (actual interface{}) {
	val, _ := this.LoadOrStoreFunc2(id, newfunc, freefunc)
	return val
}

func (this *SyncMap) LoadOrStore(id interface{}, store interface{}) (actual interface{}, loaded bool) {
	return this.storeMap.LoadOrStore(id, store)
}

func (this *SyncMap) Remove(id interface{}) {
	this.storeMap.Delete(id)
}

func (this *SyncMap) RemoveEx(id interface{}) bool {
	_, loaded := this.storeMap.LoadAndDelete(id)
	return loaded
}

func (this *SyncMap) RemoveAndGet(id interface{}) (val interface{}, ok bool) {
	return this.storeMap.LoadAndDelete(id)
}

func (this *SyncMap) AsInt32(id interface{}, def int32) int32 {
	if val, loaded := this.storeMap.Load(id); loaded {
		return GetInt32Value(val, def)
	} else {
		return def
	}
}

func (this *SyncMap) AsString(id interface{}, def string) string {
	if val, loaded := this.storeMap.Load(id); loaded {
		return GetStrValue(val, def)
	} else {
		return def
	}
}

func (this *SyncMap) Range(f func(key, value interface{}) bool) {
	this.storeMap.Range(f)
}

func (this *SyncMap) List() []interface{} {
	rval := make([]interface{}, 0, this.Count())
	this.storeMap.Range(func(key, value interface{}) bool {
		rval = append(rval, value)
		return true
	})
	return rval
}

func (this *SyncMap) ListSorted(lessfunc func(iItm, jItm interface{}) bool) []interface{} {
	rval := make([]interface{}, 0, this.Count())
	this.storeMap.Range(func(key, value interface{}) bool {
		rval = append(rval, value)
		return true
	})

	sort.Slice(rval, func(i, j int) bool {
		return lessfunc(rval[i], rval[j])
	})
	return rval
}

func (this *SyncMap) PickFirst(filter func(key, value interface{}) bool, less_sortfunc func(iItm, jItm interface{}) bool) interface{} {
	lst := this.ListEx(filter, less_sortfunc)
	if len(lst) > 0 {
		return lst[0]
	} else {
		return nil
	}
}

func (this *SyncMap) ListEx(filter func(key, value interface{}) bool, less_sortfunc func(iItm, jItm interface{}) bool) []interface{} {
	rval := make([]interface{}, 0, this.Count())
	this.storeMap.Range(func(key, value interface{}) bool {
		if filter != nil {
			if filter(key, value) {
				rval = append(rval, value)
			}
		} else {
			rval = append(rval, value)
		}

		return true
	})

	if less_sortfunc != nil {
		sort.Slice(rval, func(i, j int) bool {
			return less_sortfunc(rval[i], rval[j])
		})
	}

	return rval
}

func (this *SyncMap) ListRangeV2(filter func(key, value interface{}) bool,
	less_sortfunc func(k0, v0, k1, v1 interface{}) bool,
	rangfn func(key, val interface{}) bool) {
	rval := make([][2]interface{}, 0, this.Count())
	var appendfn = func(key, value interface{}) {
		var itm [2]interface{}
		itm[0] = key
		itm[1] = value
		rval = append(rval, itm)
	}
	this.storeMap.Range(func(key, value interface{}) bool {
		if filter != nil {
			if filter(key, value) {
				appendfn(key, value)
			}
		} else {
			appendfn(key, value)
		}

		return true
	})

	if less_sortfunc != nil {
		sort.Slice(rval, func(i, j int) bool {
			return less_sortfunc(rval[i][0], rval[i][1], rval[j][0], rval[j][1])
		})
	}

	for i := 0; i < len(rval); i++ {
		if !rangfn(rval[i][0], rval[i][1]) {
			return
		}
	}
}

func (this *SyncMap) ListRangeV3(filterflag, sortflag int, filter func(key, value interface{}) bool,
	less_sortfunc func(k0, v0, k1, v1 interface{}) bool,
	rangfn func(key, val interface{}) bool) {

	if filterflag == 0 && sortflag == 0 {
		this.storeMap.Range(rangfn)
		return
	}

	if sortflag == 0 && filterflag == 1 {
		this.storeMap.Range(func(key, value interface{}) bool {
			if filter != nil {
				if filter(key, value) {
					rangfn(key, value)
				}
			} else {
				rangfn(key, value)
			}

			return true
		})
		return
	}

	rval := make([][2]interface{}, 0, this.Count())
	var appendfn = func(key, value interface{}) {
		var itm [2]interface{}
		itm[0] = key
		itm[1] = value
		rval = append(rval, itm)
	}
	this.storeMap.Range(func(key, value interface{}) bool {
		if filter != nil {
			if filter(key, value) {
				appendfn(key, value)
			}
		} else {
			appendfn(key, value)
		}

		return true
	})

	if less_sortfunc != nil {
		sort.Slice(rval, func(i, j int) bool {
			return less_sortfunc(rval[i][0], rval[i][1], rval[j][0], rval[j][1])
		})
	}

	for i := 0; i < len(rval); i++ {
		if !rangfn(rval[i][0], rval[i][1]) {
			return
		}
	}
}

func (this *SyncMap) ValAsString(spliter string, filter func(key, value interface{}) bool, less_sortfunc func(iItm, jItm interface{}) bool) string {
	n := 0
	strs := this.ListEx(filter, less_sortfunc)
	if len(strs) > 0 {
		var sb BytesBuilder
		for i := 0; i < len(strs); i++ {
			if v1, ok := strs[i].(string); ok {
				if n > 0 {
					sb.AppendStr(spliter)
				}
				sb.AppendStr(v1)
				n++
			}
		}
		return sb.String()
	}
	return ""
}

/*
**

	ordertype 0:顺序, 1:倒序
*/
func (this *SyncMap) ValAsString2(spliter string, filter func(key, value interface{}) bool, ordertype int) string {
	return this.ValAsString(spliter, filter, func(iItm, jItm interface{}) bool {
		if ordertype == 0 {
			return iItm.(string) < jItm.(string)
		} else {
			return iItm.(string) > jItm.(string)
		}
	})
}

/*
*

	k=v;k=v;
*/
func (this *SyncMap) BatchSetFromStr(str string) {
	strs := strings.Split(str, ";")
	for i := 0; i < len(strs); i++ {
		k, v := Split2Str(strs[i], "=")
		this.Set(k, v)
	}
	return
}

func SyncMapLength(amap *sync.Map) int {
	r := 0
	amap.Range(func(key, value interface{}) bool {
		r++
		return true
	})
	return r
}
