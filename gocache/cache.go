package gocache

import (
	"sync"
	"time"
)

type CacheOperator interface {
	SetWithTTL(k string, v any, ttl time.Duration)
	Del(k string)
	Get(k string) (v any, ok bool)
	Wait()
}

type SyncMapCache struct {
	lst sync.Map
}

func (this *SyncMapCache) SetWithTTL(k string, v any, ttl time.Duration) {
	this.lst.Store(k, v)
	if ttl > 0 {
		time.AfterFunc(ttl, func() {
			this.Del(k)
		})
	}

}

func (this *SyncMapCache) Del(k string) {
	this.lst.Delete(k)
}

func (this *SyncMapCache) Get(k string) (v any, ok bool) {
	return this.lst.Load(k)
}

func (this *SyncMapCache) Wait() {

}

var (
	defCacheLk sync.RWMutex
	defCache   CacheOperator
)

func init() {
	SetDefault(nil)
}

func SetDefault(c CacheOperator) {
	if c == nil {
		c = &SyncMapCache{}
	}
	defCacheLk.Lock()
	defCache = c
	defCacheLk.Unlock()
}

func Default() CacheOperator {
	defCacheLk.RLock()
	defer defCacheLk.RUnlock()
	return defCache
}

func Set(key string, v any) {
	SetWithTTL(key, v, 0)
}

func Delete(key string) {
	Default().Del(key)
}

func SetWithTTL(key string, v any, ttl time.Duration) {
	Default().SetWithTTL(key, v, ttl)
}

func WaitSucc() {
	Default().Wait()
}

func Get(key string) (any, bool) {
	return Default().Get(key)
}

func GetOrDefault(key string, def any) any {
	if v, ok := Default().Get(key); ok {
		return v
	}
	return def
}
