package chantask

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestChanTest(t *testing.T) {
	task := NewChanTask(1024)
	var n int32 = 0
	t0 := time.Now()
	go func() {
		for task.terminated == 0 {
			task.PushTask(func(args ...interface{}) {
				atomic.AddInt32(&n, 1)
			}, 1, 2, 3)
		}
	}()

	<-time.After(time.Second * 10)
	task.Stop()
	t.Logf("n:%d, consume:%d/s, status:%s", n, int64(n)/(time.Since(t0).Milliseconds()/1000), task.StatusString())

}

func TestChanTest2(t *testing.T) {
	task := NewChanTask(1024)
	var n int32 = 0
	t0 := time.Now()
	go func() {
		for task.terminated == 0 {
			task.PushTask(func(args ...interface{}) {
				atomic.AddInt32(&n, 1)
			}, 1, 2, 3)
		}
	}()

	<-time.After(time.Second * 10)
	task.Stop()
	t.Logf("n:%d, consume:%d/s, status:%s", n, int64(n)/(time.Since(t0).Milliseconds()/1000), task.StatusString())
	task.Stop()
	t.Logf("n:%d, consume:%d/s, status:%s", n, int64(n)/(time.Since(t0).Milliseconds()/1000), task.StatusString())

}

func TestChanHubTestLife(t *testing.T) {
	var wg sync.WaitGroup
	var n int32 = 0
	go func() {
		for {
			time.Sleep(time.Second * 2)
			t.Logf("%d", atomic.LoadInt32(&n))
		}
	}()
	for i := 0; i < 1000000; i++ {
		key := fmt.Sprintf("%d", i)
		wg.Add(1)
		go func() {
			task, _ := DefaultChanTaskHub.CheckGetChanTask(key, 1024, 0)
			wg.Done()

			//n0 := atomic.LoadInt32(&n)

			go task.PushTask(func(args ...interface{}) {
				atomic.AddInt32(&n, 1)
			}, 1, 2, 3)
			runtime.Gosched()

		}()

		wg.Add(1)
		go func() {
			DefaultChanTaskHub.ReleaseChanTask(key)
			wg.Done()
		}()
	}

	wg.Wait()

	t.Logf("done")

}

func TestChanHubTest(t *testing.T) {
	t0 := time.Now()
	var n int32 = 0
	{
		task, _ := DefaultChanTaskHub.CheckGetChanTask("001", 1024, 0)
		defer DefaultChanTaskHub.ReleaseChanTask("001")

		go func() {
			for task.terminated == 0 {
				task.PushTask(func(args ...interface{}) {
					atomic.AddInt32(&n, 1)
				}, 1, 2, 3)
			}
		}()
	}

	{
		task, _ := DefaultChanTaskHub.CheckGetChanTask("002", 1024, 0)
		defer DefaultChanTaskHub.ReleaseChanTask("002")
		go func() {
			for task.terminated == 0 {
				task.PushTask(func(args ...interface{}) {
					atomic.AddInt32(&n, 1)
				}, 1, 2, 3)
			}
		}()
	}

	go func() {
		for {
			time.Sleep(time.Second * 1)
			t.Logf("n:%d, consume:%d/s", n, int64(n)/(time.Since(t0).Milliseconds()/1000))
			t.Logf("%s", DefaultChanTaskHub.StatusDetails(1))

		}

	}()

	<-time.After(time.Second * 10)
	t.Logf("n:%d, consume:%d/s", n, int64(n)/(time.Since(t0).Milliseconds()/1000))

}
