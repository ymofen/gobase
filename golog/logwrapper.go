package golog

import (
	"gitee.com/ymofen/gobase"
)

func lvl2GoLogLevel(lvl int8) GoLogLevel {
	if lvl == gobase.LogInfo {
		return LevelInfo
	} else if lvl == gobase.LogDebug {
		return LevelDebug
	} else if lvl == gobase.LogError {
		return LevelError
	} else if lvl == gobase.LogWarning {
		return LevelWarn
	}
	return gobase.LogInfo
}

func init() {
	gobase.SetLogOutput(func(calldepth int, lvl int8, sender string, s string) {
		level := lvl2GoLogLevel(lvl)
		if len(sender) == 0 {
			Default().LogMsgWithSkip(level, calldepth, s)
		} else {
			Default().LogMsgWithSkip(level, calldepth, sender+"\t"+s)
		}

	})
}
